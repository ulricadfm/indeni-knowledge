package com.indeni.server.rules.library.templatebased.fortinet

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

/**
  *
  */
case class FortinetFortiAnalyzerConnection(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "FortinetFortiAnalyzerConnection",
  ruleFriendlyName = "Fortinet Devices: FortiAnalyzer not connected",
  ruleDescription = "A Fortinet firewall needs to keep a connection with a FortiAnalyzer, otherwise certain services, such as logging, might be impacted. Indeni will alert if the connection is down.",
  metricName = "fortios-analyzer-is-connected",
  applicableMetricTag = "name",
  alertItemsHeader = "Affected Connection",
  alertDescription = "The connection between the Fortinet firewall and the FortiAnalyzer is down",
  baseRemediationText =
    """|1. Login via ssh to the Fortinet firewall and run the FortiOS command “get fortianalyzer-connectivity status” to review the connection status and remote disk usage with the FortiAnalyzer unit.
       |2. Ensure that the correct log source has been selected in the Log Settings, under GUI Preferences of the Fortinet firewall.
       |3. Check the routing and test ping connectivity between the Fortinet Firewall and the FortiAnalyzer (if icmp is allowed).
       |4. Check that the TCP port 514 is allowed between the Fortinet firewall and the FortiAnalyzer. For more information check this link: http://help.fortinet.com/fos50hlp/54/Content/FortiOS/fortigate-ports-and-protocols-54/FortiAnalyzer.htm#
       |5. Check that the DNS lookup on the Fortinet Firewall is operational in case that the FortiAnalyzer’s  FQDN needs to be resolved. Try to ping the FQDN of the FortiAnalyzer from the firewall.
       |6. Check if the firmware for the firewall and FortiAnalyzer units is compatible. Review the firmware release notes for the compatibility information.
       |7. If the problem persists, contact Fortinet Technical support at https://support.fortinet.com/ for further assistance.""".stripMargin
  )()
