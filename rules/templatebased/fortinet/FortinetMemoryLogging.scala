package com.indeni.server.rules.library.templatebased.fortinet

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}

/**
  *
  */
case class FortinetMemoryLogging(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "FortinetMemoryLogging",
  ruleFriendlyName = "Fortinet Devices: Memory logging enabled",
  ruleDescription = "Indeni will alert if logging to the system memory is enabled.",
  metricName = "fortios-memory-logging",
  alertIfDown = false,
  applicableMetricTag = "name",
  alertItemsHeader = "Logging Affected",
  alertDescription = "The memory logging is enabled. When the logging buffer is full, the oldest messages will be overwritten. When the system restarts, all log entries stored in the system memory will be lost. So memory logging can only be used temporarily and other logging facility should be used for long time solution.",
  baseRemediationText = 
    """Turn off memory logging as soon as possible.
      |1. Login via ssh to the Fortinet firewall and run the FortiOS command “get log memory setting” to review the logging memory status. If the FortiGate unit has a hard disk, it is enabled by default to store logs. If the FortiGate unit has only flash memory, disk logging is disabled by default.
      |2. Login via https to the Fortinet firewall and go to the menu System > Dashboard > Status. Look at the system resources widget to review the Memory utilization graph. If the memory utilization is high then it is recommended to disable the logging to memory setting. Use the FortiOS commands "execute filter log device X", "execute log filter category Y" and  "execute log delete"  to clear the logs.
      |3. Run the FortiOS command “execute log filter device“ to get a list of the supported log devices. Consider storing logs to Syslog, FortiAnalyzer or FortiCloud instead of memory or hard disk.
      |4. If logging to memory is the only option then it is a good practice to manually set the warning thresholds and the max memory log buffer size under the “config log memory global-setting” FortiOS CLI.
      |5. For more information review the Fortinet Handbook: https://docs.fortinet.com/uploaded/files/3421/logging-reporting-54.pdf""".stripMargin
                
)(
)
