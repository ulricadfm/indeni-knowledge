package com.indeni.server.rules.library.templatebased.radware

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.NearingCapacityTemplateRule

/**
  *
  */
case class radware_filters_table_limit(context: RuleContext) extends NearingCapacityTemplateRule(context,
  ruleName = "radware_filters_table_limit",
  ruleFriendlyName = "Radware Alteon: Filters table usage high",
  ruleDescription = "Alteon devices allow for the configuration of filters in a table. indeni will alert prior to the table reaching its limit.",
  usageMetricName = "filters-usage",
  limitMetricName = "filters-limit",
  threshold = 80.0,
  alertDescriptionFormat = "The filter table usage is %.0f where the limit is %.0f.",
  baseRemediationText = "Review the filters defined to determine if some of them may be removed.")()
