package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SnapshotComparisonTemplateRule

/**
  *
  */
case class cross_vendor_compare_md5_across_cluster(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "cross_vendor_compare_md5_across_cluster",
  ruleFriendlyName = "Clustered Devices: Critical configuration files mismatch across cluster members",
  ruleDescription = "indeni will identify when two devices are part of a cluster and alert if critical configuration files are different.",
  metricName = "file-md5sum",
  applicableMetricTag = "path",
  isArray = false,
  alertDescription = "Devices that are part of a cluster must have the same settings in their critical configuration files. Review the differences below.",
  baseRemediationText = """Correct any differences found to ensure a complete match between device members.""",
  alertItemsHeader = "Mismatching Files",
  includeSnapshotDiff = false)()
