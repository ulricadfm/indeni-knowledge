package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}

/**
  *
  */
case class cross_vendor_hardware_element_status(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cross_vendor_hardware_element_status",
  ruleFriendlyName = "All Devices: Hardware element down",
  ruleDescription = "Alert if any hardware elements are not operating correctly.",
  metricName = "hardware-element-status",
  applicableMetricTag = "name",
  alertItemsHeader = "Hardware Elements Affected",
  alertDescription = "The hardware elements listed below are not operating correctly.",
  baseRemediationText = "Troubleshoot the hardware element as soon as possible.")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Use the "show environment [ fan | power | temperature ]" NX-OS command to display information about the hardware environment status.
      |2. For more information please review the following CISCO Nexus HW Troubleshooting (fan/PS/Temp/Xbar/SUP) guide: https://www.cisco.com/c/en/us/support/docs/switches/nexus-7000-series-switches/200148-Troubleshooting-N7K-HW-fan-PS-Temp-Xbar.html
      |
      |In case of issue to the transceiver:
      |1. Use the "show interface transceiver detailed" NX-OS command to display detailed information for transceiver interfaces.
      |2. Use the "show interface transceiver calibrations" NX-OS  command to display calibration information for transceiver interfaces""".stripMargin,
  ConditionalRemediationSteps.VENDOR_FORTINET ->
    """
      |1. Login via ssh to the Fortinet firewall and run the FortiOS command "exec sensor list" to review the status of the hardware components and temperature
        |>>> thresholds. When the flag to the command output is set to 0, the component is working correctly and when flag is set to 1, the component has a problem.
        |>>> The FortiOS command "execute sensor detail" will show extra information such as the low/high thresholds. More details can be found here:
        |>>> http://kb.fortinet.com/kb/viewContent.do?externalId=FD36793&sliceId=1
      |2. Consider running the fotrinet hardware diagnostics commands. While they do not detect all hardware malfunctions, tests for the most common hardware
        |>>> problems are performed. More details can be found here:
        |- http://kb.fortinet.com/kb/viewContent.do?externalId=FD39581&sliceId=1
        |- http://kb.fortinet.com/kb/documentLink.do?externalID=FD34745
      |3. It is recommended that any failed fan or power supply unit should be replaced immediately.
      |4. The cooling system for the devices should be installed to avoid overheat.
      |5. If the problem persists, contact Fortinet Technical support at https://support.fortinet.com/ for further assistance.""".stripMargin.replaceAll("\n>>>", "")
)
