package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.ruleengine.expressions.conditions.Equals
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, MultiSnapshotValueCheckTemplateRule, NumericThresholdOnDoubleMetricWithItemsTemplateRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity


/**
  * Created by tomas on 18/07/17.
  */


case class CrossVendorRepeatedFailedLoginAttempts(context: RuleContext) extends NumericThresholdOnDoubleMetricWithItemsTemplateRule(context,
  ruleName = "cross_vendor_repeated_failed_login_attempts",
  ruleFriendlyName = "All Devices: Repeated failed login attempts by a user",
  ruleDescription = "Alert if a user is repeatedly trying to login unsuccessfully during the last hour.",
  metricName = "failed-logins",
  threshold = 3.0,
  applicableMetricTag = "username",
  alertItemsHeader = "Failed Login Attempts",
  alertItemDescriptionFormat = "A user has made %.0f failed logins during the last hour.",
  alertDescription = "Indeni has detected repeated password guessing against the device in the last hour. This may be due to penetration testing, a user attempting many times with an incorrect password or user name, or malicious attempt to log on to the device.",
  baseRemediationText = "Investigate from where the logins are originating from and take action to block the attempts if necessary."
)(ConditionalRemediationSteps.VENDOR_CP -> "Check \"/var/log/secure\" on the device."
)
