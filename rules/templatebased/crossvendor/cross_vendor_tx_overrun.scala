package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, NearingCapacityWithItemsTemplateRule}

/**
  *
  */
case class CrossVendorTxOverrun(context: RuleContext) extends NearingCapacityWithItemsTemplateRule(context,
  ruleName = "cross_vendor_tx_overrun",
  ruleFriendlyName = "All Devices: TX packets overrun",
  ruleDescription = "Indeni tracks the number of packets that had issues and alerts if the ratio is too high.",
  usageMetricName = "network-interface-tx-overruns",
  limitMetricName = "network-interface-tx-packets",
  applicableMetricTag = "name",
  threshold = 0.5,
  minimumValueToAlert = 100.0, // We don't want to alert if the number of error packets is really low
  alertDescription = "Some network interfaces and ports are experiencing a high overrun rate. Review the ports below.",
  alertItemDescriptionFormat = "%.0f packets overrun out of a total of %.0f transmitted.",
  baseRemediationText = "Packet overruns usually occur when there are too many packets being inserted into the port's memory buffer, faster than the rate at which the kernel is able to process them.",
  alertItemsHeader = "Affected Ports")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
       |In a small number of cases, the overrun counter may be incremented because of a software defect. However, in the majority of cases, it indicates that the receiving capability of the interface was exceeded. Nothing can be done on the device that reports overruns. If possible, the rate that frames are coming should be controlled at the remote end of the connection.
       |1. Run the "show interface" command to review the interface overrun & underun counters and the bitrate. Consider to configure the "load-interval 30" interface sub command to improve the accuracy of the interface measurements.
       |2. If the number of overruns is high, the hardware should be upgraded.
       |In case of high bandwidth utilization:
       |1. Run the "show interface" command to review the interface counters and the bitrate. Consider to configure the "load-interval 30" interface sub command to improve the accuracy of the interface measurements.
       |2. If the interface bitrate is too high and close to the upper bandwidth limit consider to use multiple links with the port-channel technology or upgrade the bandwidth of the link
       |3. Consider to implement QoS in case of high bandwidth utilization.""".stripMargin,
  ConditionalRemediationSteps.VENDOR_JUNIPER ->
    """|The issue can not be resolved on the device that reports overruns. The rate that frames are coming should be controlled at the remote end of the connection.
       |1. Run the “show interface extensive” command to review the interface overrun counters.  
       |2. If the number of overruns is high, consider upgrading the hardware.
       |3. Review the following article on Juniper tech support site: <a target="_blank" href="https://www.juniper.net/documentation/en_US/junos/topics/reference/command-summary/show-interfaces-security.html#jd0e1772">Operational Commands</a>""".stripMargin,
  ConditionalRemediationSteps.VENDOR_FORTINET ->
    """
       |1. Run "diag hardware deviceinfo nic <interface>" command to display a list of hardware related error names and values. Review  the next link for more details: http://help.fortinet.com/fos50hlp/54/Content/FortiOS/fortigate-toubleshooting-54/troubleshooting_tools.htm
       |2. Run command "fnsysctl cat /proc/net/dev" to get a summary of the interface statistics.
       |3. Check for speed and duplex mismatch in the interface settings on both sides of a cable, and check for a damaged cable. Review the next link for more info: http://kb.fortinet.com/kb/documentLink.do?externalID=10653""".stripMargin
)
