package com.indeni.server.rules.library.templatebased.paloaltonetworks

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.CounterIncreaseTemplateRule

/**
  *
  */
case class palo_alto_vpn_auth_errors(context: RuleContext) extends CounterIncreaseTemplateRule(context,
  ruleName = "palo_alto_vpn_auth_errors",
  ruleFriendlyName = "Palo Alto Networks Firewalls: VPN dropping packets due to authentication errors",
  ruleDescription = "indeni tracks critical error metrics for VPN tunnels and alerts when these are increasing.",
  metricName = "vpn-tunnel-authentication-errors",
  applicableMetricTag = "peerip",
  alertDescription = "The VPNs listed below are experiencing packet authentication errors. This is probably due to a configuration issue.",
  alertRemediationSteps = "Review the configurations on both sides of the tunnel.",
  alertItemsHeader = "Affected VPN Tunnels"
)()
