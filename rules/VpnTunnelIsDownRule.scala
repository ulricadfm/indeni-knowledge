package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine.expressions.conditions.GreaterThan
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.ts.TimeSinceLastValueExpression
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan
import com.indeni.server.rules.library.{ConditionalRemediationSteps, RuleHelper}


case class VpnTunnelIsDownRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private val highThresholdParameterName = "High_Threshold_of_VPN_Downtime"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "High Threshold of VPN Tunnel Downtime",
    "If a VPN tunnel is down for at least this amount of time, an alert will be issued.",
    UIType.TIMESPAN,
    TimeSpan.fromMinutes(15))

  override val metadata: RuleMetadata = RuleMetadata.builder("cross_vendor_vpn_tunnel_down", "Firewall Devices: VPN tunnel(s) down",
    "Indeni will alert if one or more VPN tunnels is down.", AlertSeverity.ERROR).configParameter(highThresholdParameter).build()

  override def expressionTree: StatusTreeExpression = {
    val threshold = getParameterTimeSpanForRule(highThresholdParameter).noneable

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(

        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set("peerip", "name"), True),

        // The condition which, if true, we have an issue. Checked against the metrics we've collected
        // NOTE: We are looking to see if we found an UP status for the VPN tunnel in the last x minutes (defined by threshold).
        // If the VPN is down, one of the two will occur:
        // 1. We still stop getting metrics for the VPN (we will keep getting "None" in the time series) until the "1.0" we used to have is too old, then we'll alert.
        // 2. We will start getting "0.0" values. In this case, we will still wait until the most recent "1.0" is too old (older than the threshold).
        //    Why do we wait until the 1.0 is too old instead of just saying "We see 0.0, VPN is down!"? Because it allows for more consistency. This way
        //    in both cases, it takes us *threshold* minutes to alert. Besides, many tunnels go down and up regularly and we don't want to alert on a tunnel
        //    that's down for a short period of time.
        StatusTreeExpression(
          SelectTimeSeriesExpression[Double](context.tsDao, Set("vpn-tunnel-state"), ConstantExpression(TimeSpan.fromDays(1)), denseOnly = false),
          GreaterThan(TimeSinceLastValueExpression(TimeSeriesExpression("vpn-tunnel-state"), Some(1.0)), threshold)

        ).withSecondaryInfo(
          scopableStringFormatExpression("${scope(\"name\")} (${scope(\"peerip\")})"),
          scopableStringFormatExpression("This tunnel has been down for more than %s.", threshold),
          title = "VPN Tunnels Affected"
        ).asCondition()
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("One or more VPN tunnels are down."),
      ConditionalRemediationSteps("Review the cause for the tunnels being down.",
        ConditionalRemediationSteps.VENDOR_CP -> "Indeni uses the \"vpn tu\" command on the firewall to determine gateway status. Open SmartView Tracker and look for recent logs pertaining to the VPN peers listed above. Consider reading: <a target=\"_blank\" href=\"https://indeni.com/check-point-firewalls-troubleshoot-a-vpn-connection/\">How to Troubleshoot Check Point Firewall VPN Connection</a>",
        ConditionalRemediationSteps.VENDOR_PANOS -> "Review <a target=\"_blank\" href=\"https://live.paloaltonetworks.com/t5/Management-Articles/How-to-Troubleshoot-IPSec-VPN-connectivity-issues/ta-p/59187\">How to Troubleshoot IPSec VPN connectivity issues</a>",
        ConditionalRemediationSteps.VENDOR_JUNIPER ->
          """|Run "show security ipsec inactive-tunnels" command to review inactive tunnels and tunnel down reasons.
             |2. Run "show security ipsec security-associations brief [detail]" to check if Phase 1 and Phase 2 are up.
             |3. Check the Phase 1 and Phase 2 configuration. Ensure they are matched on both ends.
             |4. Check if any filtering is applied to access-list, policy or NAT.
             |5. Check the routes to the remote peer.
             |6. Check the logs for VPN tunnel reporting
             |7. Consider enabling VPN monitoring for the tunnel status.
             |8. Consider setting traceoptions by running "set security ike traceoptions file vpn.tr size 5m files 5 world-readable" command for more detailed information.
             |9. Review the following articles on Juniper tech support site: <a target="_blank" href="https://kb.juniper.net/InfoCenter/index?page=content&id=KB10100&actp=METADATA">How to troubleshoot a VPN tunnel that is down or not active</a>
             |9. <a target="_blank" href="https://www.juniper.net/documentation/en_US/junos/topics/concept/secuity-vpn-monitoring-understanding.html">Understanding VPN Monitoring</a>
             |10. Contact Juniper Networks Technical Assistance Center (JTAC) if further assistance is required.""".stripMargin
      )
    )
  }
}


