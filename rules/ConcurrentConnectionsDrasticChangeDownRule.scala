package com.indeni.server.rules.library
/*
import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.ResultsFound
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.ruleengine.expressions.{DownSlopeDetection, Expression}
import com.indeni.server.params._
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan
import com.indeni.server.rules.library.{ConditionalRemediationSteps, RuleHelper}


/**
  * Created by amir on 04/02/2016.
  */
case class ConcurrentConnectionsDrasticChangeDownRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private val reviewedTimeframeParameter = new ParameterDefinition("reviewed_timeframe",
    "",
    "Reviewed Timeframe",
    "The window of time we review to detect changes.",
    ParameterDefinition.UIType.TIMESPAN,
    TimeSpan.fromMinutes(120))


  override val metadata: RuleMetadata = RuleMetadata.builder("concurrent_connections_drastic_change_down", "All Devices: Drastic drop in concurrent connections", "Indeni will alert when a drastic drop is detected in the number of concurrent connections.", AlertSeverity.ERROR).configParameter(reviewedTimeframeParameter).build()

  override def expressionTree: StatusTreeExpression = {
    val numOfConnectionsTimeSeries = TimeSeriesExpression[Double]("concurrent-connections")
    val lastChange = DownSlopeDetection(numOfConnectionsTimeSeries).withLazy

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      StatusTreeExpression(
        // The time-series we check the test condition against:
        SelectTimeSeriesExpression[Double](context.tsDao, Set("concurrent-connections"), historyLength = getParameterTimeSpanForRule(reviewedTimeframeParameter)),

        // The condition which, if true, we have an issue. Checked against the time-series we've collected
        ResultsFound(lastChange)

        // The Alert Item to add for this specific item
      ).withRootInfo(
        getHeadline(),
        scopableStringFormatExpression("A drastic drop down has been detected in the number of concurrent connections. %s.", new ScopableExpression[Any] {

          override protected def evalWithScope(time: Long, scope: Scope): String = s"Changed from ${lastChange.eval(time).head._1} to ${lastChange.eval(time).head._2}"

          override def args: Set[Expression[_]] = Set(lastChange)
        }),
        ConditionalRemediationSteps("Each device has a limit for concurrent sessions or connections based on the hardware capacity. Exceeding this limit will cause traffic drops. Review why this may be happening.",
          ConditionalRemediationSteps.VENDOR_JUNIPER ->
            """|1. Run "show security flow session summary" command to review the number of sessions.
               |2. Check the session table for abnormally high number of concurrent connections.
               |3. Consider upgrading the hardware with capacity to handle increased number of sessions.
               |4. Consider setting session termination based on aging or the session table being full.
               |4. Review the following articles on Juniper TechLibrary: <a target="_blank" href="https://www.juniper.net/documentation/en_US/junos/topics/concept/security-session-capacity-device-expanding.html">Expanding Session Capacity by Device</a>
               |<a target="_blank" href="https://www.juniper.net/documentation/en_US/junos/topics/example/session-termination-for-srx-series-controlling-cli.html">Example: Controlling Session Termination for SRX Series Services Gateways</a>
               |5. Contact Juniper Networks Technical Assistance Center (JTAC) if further assistance is required.""".stripMargin)
      ).asCondition()
    ).withoutInfo()
  }
}
*/