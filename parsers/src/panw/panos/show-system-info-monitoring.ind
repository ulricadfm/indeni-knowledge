#! META
name: panos-show-system-info-monitoring
description: fetch system info for monitoring
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: paloaltonetworks
    os.name: panos
    
#! COMMENTS
uptime-seconds:
    why: |
        When a monitoring system loses connectivity to a device, it may be difficult for it to determine whether the device restarted, or is simply unreachable. To deal with that, the uptime is tracked. The uptime of a device resetting is a clear indicator of a device restart. 
    how: |
        This alert uses the Palo Alto Networks API to retrieve the current uptime (the equivalent of running "show system info" in CLI).
    without-indeni: |
        An administrator will normally find out that a device has restarted when a service outage actually occurs.
    can-with-snmp: true
    can-with-syslog: true
software-eos-date:
    why: |
        Ensuring the software being used is always within the vendor's list of supported versions is critical. Otherwise, during a critical issue, the vendor may decline to provide technical support. Palo Alto Networks posts the list of supported software on their website ( https://www.paloaltonetworks.com/services/support/end-of-life-announcements/end-of-life-summary ). indeni tracks that list and updates this script to match.
    how: |
        This script uses the Palo Alto Networks API to retrieve the current software version (the equivalent of running "show system info" in CLI) and based on the software version and the Palo Alto Networks provided information at https://www.paloaltonetworks.com/services/support/end-of-life-announcements/end-of-life-summary the correct end of support date is used.
    without-indeni: |
        Manual tracking by an administrator is usually the only method for knowing when a given device may be nearing its software end of support and is in need of upgrading.
    can-with-snmp: false
    can-with-syslog: false
hardware-eos-date:
    why: |
        Ensuring the hardware being used is always within the vendor's list of supported models is critical. Otherwise, during a critical issue, the vendor may decline to provide technical support ( https://www.paloaltonetworks.com/services/support/end-of-life-announcements/hardware-end-of-life-dates ). indeni tracks that list and updates this script to match.
    how: |
        This script uses the Palo Alto Networks API to retrieve the current hardware model (the equivalent of running "show system info" in CLI) and based on the model and the Palo Alto Networks provided information at https://www.paloaltonetworks.com/services/support/end-of-life-announcements/hardware-end-of-life-dates the correct end of support date is used.
    without-indeni: |
        Manual tracking by an administrator is usually the only method for knowing when a given device may be nearing its end of support and is in need of replacement.
    can-with-snmp: false
    can-with-syslog: false
current-datetime:
    why: |
        The clock of a Palo Alto Networks firewall should always be accurate, as inaccuracies may result in issues with some features, as well as causing a mess in log analysis. Normally, administrators are encouraged to use NTP to keep the clock in sync (and indeni has a script for verifying NTP is working). If NTP is not used, one should still verify that the clock is set correctly.
    how: |
        This script uses the Palo Alto Networks API to retrieve the current date and time (the equivalent of running "show system info" in CLI). indeni then compares the result to its own clock to find possible discrepancies.
    without-indeni: |
        Manual tracking by an administrator is usually the only method for knowing when a given device's clock may be off.
    can-with-snmp: false
    can-with-syslog: false
os-version:
    why: |
        Two or more devices which operate as part of a single cluster must be running the same version of software.
    how: |
        This script uses the Palo Alto Networks API to retrieve the software version installed on the device. indeni then compares the result to the same script run on other members of the same cluster.
    without-indeni: |
        Manual tracking by an administrator is usually the only method for knowing when two devices are not running the same version of software.
    can-with-snmp: false
    can-with-syslog: false
model:
    why: |
        Two or more devices which operate as part of a single cluster must be running on the same hardware.
    how: |
        This script uses the Palo Alto Networks API to retrieve the hardware model of the device. indeni then compares the result to the same script run on other members of the same cluster.
    without-indeni: |
        Manual tracking by an administrator is usually the only method for knowing when two devices are not running on the same hardware.
    can-with-snmp: false
    can-with-syslog: false
vendor:
    skip-documentation: true
serial-numbers:
    skip-documentation: true
os-name:
    why: |
        Two or more devices which operate as part of a single cluster must be running the same version of software.
    how: |
        This script uses the Palo Alto Networks API to retrieve the software name and version installed on the device. indeni then compares the result to the same script run on other members of the same cluster.
    without-indeni: |
        Manual tracking by an administrator is usually the only method for knowing when two devices are not running the same version of software.
    can-with-snmp: false
    can-with-syslog: false
panw-panos-panorama-cert-expr:
    why: |
        On April 3rd, 2017, Palo Alto Networks notified all customers that an upgrade to Panorama may be necessary to ensure uninterrupted communications between the Panorama device and the firewalls. Knowing which Panorama installations are affected is important.
    how: |
        This script uses the Palo Alto Networks API to retrieve the software name and version installed on the device.
    without-indeni: |
        An administrator would need to be aware of the issue and manually look at the software version of all Panorama installations.
    can-with-snmp: false
    can-with-syslog: false


#! REMOTE::HTTP
url: /api?type=op&cmd=<show><system><info></info></system></show>&key=${api-key}
protocol: HTTPS

#! PARSER::XML
_metrics:
    -
        _tags:
            "im.name":
                _constant: "uptime-seconds"
            "live-config":
               _constant: "true"
            "display-name":
                _constant: "Uptime"
            "im.dstype.displayType":
                _constant: "duration"
        _temp:
            "uptime":
                _text: "/response/result/system/uptime"
        _transform:
            _value.double: | 
               {
                   # 230 days, 16:57:34
                split(temp("uptime"), vals, " ")
                if (arraylen(vals) == 3  && vals[2] == "days,") {
                    # 230 days, 16:57:34
                    days = vals[1]
                    split(vals[3], timevals, ":")
                    hours = timevals[1]
                    minutes = timevals[2]
                    seconds = timevals[3]
                    uptime = (days * 3600 * 24) + (hours * 3600) + (minutes * 60) + seconds
                    print uptime
                }
               }
    -
        _value.complex:
            "name":
                _constant: "Device"
            "serial-number":
                _text: "/response/result/system/serial"
        _tags:
            "im.name":
                _constant: "serial-numbers"
            "live-config":
               _constant: "true"
            "display-name":
                _constant: "Serial Numbers"
        _value: complex-array
    -
        _value.complex:
            "value":
                _constant: "Palo Alto Networks"
        _tags:
            "im.name":
                _constant: "vendor"
            "live-config":
               _constant: "true"
            "display-name":
                _constant: " Overview "
    -
        _value.complex:
            "value":
                _constant: "PAN-OS"
        _tags:
            "im.name":
                _constant: "os-name"
            "live-config":
               _constant: "true"
            "display-name":
                _constant: " Overview "
    -
        _value.complex:
            "value":
                _text: "/response/result/system/sw-version"
        _tags:
            "im.name":
                _constant: "os-version"
            "live-config":
               _constant: "true"
            "display-name":
                _constant: " Overview "
    -
        _temp:
            swversion:
                _text: "/response/result/system/sw-version"
            "model":
                _text: "/response/result/system/model"
        _tags:
            "im.name":
                _constant: "panw-panos-panorama-cert-expr"
        _transform:
            _value.complex:
                "value": |
                    {
                        if (temp("model") ~ /.*anorama/) {
                            split(temp("swversion"), versionparts, "\\.")
                            if (arraylen(versionparts) == 3) {
                                if (versionparts[1] == "7" && versionparts[2] == "1" && ((versionparts[3] + 0) < 9)) {
                                    print "true"
                                } else if (versionparts[1] == "7" && versionparts[2] == "0" && ((versionparts[3] + 0) < 15)) {
                                    print "true"
                                } else if (versionparts[1] == "6" && versionparts[2] == "1" && ((versionparts[3] + 0) < 17)) {
                                    print "true"
                                } else {
                                    print "false"
                                }
                            } else {
                                    print "false"
                            }
                        } else {
                            print "false"
                        }
                    }
    -
        _value.complex:
            "value":
                _text: "/response/result/system/model"
        _tags:
            "im.name":
                _constant: "model"
            "live-config":
               _constant: "true"
            "display-name":
                _constant: " Overview "
    -
        _tags:
            "im.name":
                _constant: "software-eos-date"
            "live-config":
               _constant: "true"
            "display-name":
                _constant: "End of Support - Software"
            "im.dstype.displayType":
                _constant: "date"
        _temp:
            "sw-version":
                _text: >
                    /response/result/system/sw-version[starts-with(., '5.0.') or starts-with(., '5.1.') or starts-with(., '6.0.') or starts-with(., '6.1.') or starts-with(., '7.0.') or starts-with(., '7.1.')]
        _transform:
            _value.double: | 
               {
                # 6.1.2
                versionstring=temp("sw-version")
                eos=0
                if (match(versionstring, "^5\.0.*")) {
                    eos=date(2016,11,13)
                } else if (match(versionstring, "^5\.1.*")) {
                    eos=date(2017,5,9)
                } else if (match(versionstring, "^6\.0.*")) {
                    eos=date(2017,1,19)
                } else if (match(versionstring, "^6\.1.*")) {
                    eos=date(2018,10,25)
                } else if (match(versionstring, "^7\.0.*")) {
                    eos=date(2017,6,4)
                } else if (match(versionstring, "^7\.1.*")) {
                    eos=date(2020,3,29)
                } 

                if (eos != 0) {
                    print eos
                } else {
                    print "ERROR"
                }
               }
    -
        _tags:
            "im.name":
                _constant: "hardware-eos-date"
            "live-config":
               _constant: "true"
            "display-name":
                _constant: "End of Support - Hardware"
            "im.dstype.displayType":
                _constant: "date"
        _temp:
            "model":
                _text: "/response/result/system/model[(starts-with(., 'PA-20') or starts-with(., 'PA-40')) and string-length(.) = 7]"
        _transform:
            _value.double: | 
               {
                # PA-200
                modelstring=temp("model")
                
                eos=0
                if (match(modelstring, "PA-20[0-9][0-9]")) {
                    eos=date(2020,4,30)
                } else if (match(modelstring, "PA-40[0-9][0-9]")) {
                    eos=date(2019,4,30)
                }

                if (eos != 0) {
                    print eos
                } else {
                    print "0"
                }
               }
    -
        _tags:
            "im.name":
                _constant: "current-datetime"
            "live-config":
               _constant: "true"
            "display-name":
                _constant: "Current Time"
            "im.dstype.displayType":
                _constant: "date"
        _temp:
            "time":
                _text: "/response/result/system/time"
        _transform:
            _value.double: | 
               {
                timestring=temp("time")
                gsub(/  /, " ", timestring) # We may have double space before the date number
                split(timestring, vals, " ")
                if (arraylen(vals) > 4) {
                    split(vals[4], timevals, ":")
                    currenttime = datetime(vals[5], parseMonthThreeLetter(vals[2]), vals[3], timevals[1], timevals[2], timevals[3])
                    print currenttime
                }
               }
    -
        _tags:
            "im.name":
                _constant: "panw-installed-app-release-date"
            "live-config":
               _constant: "true"
            "display-name":
                _constant: "Application Packages - Currently Installed Package"
            "im.dstype.displayType":
                _constant: "date"
        _temp:
            "releasedate":
                _text: "/response/result/system/app-release-date"
        _transform:
            _value.double: | 
               {
                # 2015/03/03  19:53:18
                releasedatestring=temp("releasedate")
                gsub(/  /, " ", releasedatestring) # We may have double space before the hour
                split(releasedatestring, parts, " ")
                if (arraylen(parts) == 2) {
                    split(parts[1], datevals, "/")
                    split(parts[2], timevals, ":")
                    print datetime(datevals[1], datevals[2], datevals[3], timevals[1], timevals[2], timevals[3])
                } else {
                    print arraylen(parts)
                }
               }
