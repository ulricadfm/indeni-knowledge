#! META
name: panos-debug-status-mprelay-show
description: grab the debug status of mprelay
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: paloaltonetworks
    os.name: panos
    product: firewall
    this_tag_disables_this_script: this_is_intentional

#! COMMENTS
debug-status:
    skip-documentation: true

#! REMOTE::SSH
debug mprelay show

#! PARSER::AWK
BEGIN {
	# For each flag this command supports, we list what a "normal" value is
	normal["sw.mprelay.s1.dp0.debug"] = "info"
	normal["sw.mprelay.s1.dp1.debug"] = "info"
	normal["sw.mprelay.s1.dp2.debug"] = "info"
	normal["sw.mprelay.s1.dp3.debug"] = "info"
	normal["sw.mprelay.s1.dp4.debug"] = "info"
	normal["sw.mprelay.s1.dp5.debug"] = "info"
	normal["sw.mprelay.s1.dp6.debug"] = "info"
	normal["sw.mprelay.s1.dp7.debug"] = "info"
	normal["sw.mprelay.s1.dp8.debug"] = "info"
	normal["sw.mprelay.s1.dp9.debug"] = "info"
}

# Debug status lines look like this:
# sw.ikedaemon.debug.pcap: False
# note that the value can be all kinds of things depending on what the debug element is
/[a-zA-Z\.\'\-\s]+\:\s*(\S)+/ {
	writeDebug($0)
	gsub(/(\{|\}|')/, "", $0) # Sometimes the output can be in JSON format like "{'md.apps.s1.mp.cfg.debug-level': 'info'}"
	flag = $1
	sub(/:$/, "", flag)
	debugtags["name"] = flag

	state = 1
	if (normal[flag] == $2) {
		state = 0
	}
	writeDoubleMetric("debug-status", debugtags,"gauge",3600,state)
}

END {
}
