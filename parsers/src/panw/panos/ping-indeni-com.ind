#! META
name: panos-ping-indeni-com
description: check to see if DNS resolution is working 
type: monitoring
monitoring_interval: 30 minutes
requires:
    vendor: paloaltonetworks
    os.name: panos

#! COMMENTS
dns-server-state:
    why: |
        Some services on a Palo Alto Networks firewall require a working DNS connection. For example, the FQDN objects require DNS connectivity (see https://live.paloaltonetworks.com/t5/Configuration-Articles/How-to-Configure-and-Test-FQDN-Objects/ta-p/61903).
    how: |
        This script logs into the Palo Alto Networks firewall through SSH attempts to ping www.indeni.com. In the process of that ping, it also forces the firewall to resolve "www.indeni.com" to an IP address. A failure to ping www.indeni.com indicates that the DNS server is not responding, or that connectivity to the Internet has been severed.
    without-indeni: |
        An administrator would need to write a script to poll their firewalls for the data (force a resolution of a hostname), or simply troubleshoot once an issue occurs.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
ping count 1 host www.indeni.com

#! PARSER::AWK
BEGIN {
	dns_success=0
}

# Let's see if it managed to translate the domain to IP:
# PING indeni.com (109.199.106.156) 56(84) bytes of data.
/PING indeni.com \([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\)/ {
	dns_success=1
}

END {
	dnstags["name"] = "Currently configured DNS server(s)"
	writeDoubleMetricWithLiveConfig("dns-server-state",dnstags,"gauge",300,dns_success, "DNS Servers - State", "state", "name")
}
