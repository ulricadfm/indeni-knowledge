#! META
name: panos-debug-status-sslmgr-show-setting
description: grab the debug status of SSL MGR
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: paloaltonetworks
    os.name: panos
    product: firewall
    this_tag_disables_this_script: this_is_intentional

#! COMMENTS
debug-status:
    skip-documentation: true

#! REMOTE::SSH
debug sslmgr show setting

#! PARSER::AWK
BEGIN {
	normal["sw.sslmgr.runtime.debug.level"] = "info"
}

# Debug status lines look like this:
# sw.ikedaemon.debug.pcap: False
# note that the value can be all kinds of things depending on what the debug element is
/[a-zA-Z\.\'\-\s]+\:\s*(\S)+/ {
	writeDebug($0)
	gsub(/(\{|\}|')/, "", $0) # Sometimes the output can be in JSON format like "{'md.apps.s1.mp.cfg.debug-level': 'info'}"
	flag = $1
	sub(/:$/, "", flag)
	debugtags["name"] = flag

	state = 1
	if (normal[flag] == $2) {
		state = 0
	}
	writeDoubleMetric("debug-status", debugtags,"gauge",3600,state)
}

END {
}
