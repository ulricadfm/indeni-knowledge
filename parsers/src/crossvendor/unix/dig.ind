#! META
name: unix-dig
description: run "dig www.indeni.com"
type: monitoring
monitoring_interval: 10 minutes
requires:
    or:
        -
            linux-based: "true"
        -
            freebsd-based: "true"

#! COMMENTS
dns-server-state:
    why: |
        Even though DNS servers are configured, that does not guarantee that they work. Many products require a fully functional DNS server being set.
    how: |
        Using the built-in "dig" command, each configured DNS server on the device is sent a query to resolve www.indeni.com
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        This can only be tested from the command line interface. However if DNS is not working other alerts might appear, for example failure to update the devices softwre packages, however it will not be clear that they are related to DNS issues.

dns-server-state:
    why: |
        Even though DNS servers are configured, that does not guarantee that they are fast enough. Slow DNS servers could indicate other underlying issues on network or server side.
    how: |
        Using the built-in "dig" command, each configured DNS server on the device is sent a query to resolve www.indeni.com and response time is measured.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        This can only be tested from the command line interface. However if DNS is not working other alerts might appear, for example failure to update the devices softwre packages, however it will not be clear that they are related to DNS issues.

dns-servers:
    why: |
        DNS allows a device to resolve a name to an IP address. For example, an application or website may be associated with many IP's and DNS allows the client to use a name or FQDN to reach it. If a device is clustered then it would be expected to have the same DNS servers configured on all members of the cluster.
    how: |
        Using the built-in "dig" command, each configured DNS server on the device is listed.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Showing the configured DNS servers is normally only available on the CLI or via WebUI.

#! REMOTE::SSH
# We go over to bash because in FreeBSD we may be in csh, which doesn't support this format of "while"
${nice-path} -n 15 bash -c 'cat /etc/resolv.conf | grep nameserver | sed "s/.* //" |while read server; do echo DNSserver $server && dig @$server www.indeni.com +noall +stats +answer +time=1; done'

#! PARSER::AWK

############
# Script explanation: Decrease timeout (+time=1), otherwise the script times out if there are too many servers that are not reachable. One second means that for each DNS server it will make three attempts and wait 1 second for each.
###########

# DNSserver 8.8.8.8
/DNSserver/ {
	server=$NF
	serverStatus[server] = 0
	idns++
	dns[idns, "ipaddress"] = server
}

# ;; Query time: 12 msec
/Query time/ {
	querytime=$(NF-1)
	serverStatus[server] = 1
	response[server] = querytime
}


END {
	for (id in serverStatus) {
		dnstags["dns-server"] = id
		t["name"] = id
		if (serverStatus[id] == 1) {
			writeDoubleMetricWithLiveConfig("dns-response-time", dnstags, "gauge", "60", response[id], "DNS Response Time (Average)", "number", "dns-server")
		}
		writeDoubleMetricWithLiveConfig("dns-server-state", t, "gauge", "600", serverStatus[id], "DNS Servers", "state", "name")
		writeComplexMetricObjectArrayWithLiveConfig("dns-servers", null, dns, "DNS Servers")
	}
}