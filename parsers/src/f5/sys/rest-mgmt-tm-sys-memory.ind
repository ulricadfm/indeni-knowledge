#! META
name: f5-rest-mgmt-tm-sys-memory
description: Determine tmm memory usage
type: monitoring
monitoring_interval: 1 minute
includes_resource_data: true
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"
    shell: "bash"
    vsx:
        neq: "true"

#! COMMENTS
memory-free-kbytes:
    skip-documentation: true
memory-total-kbytes:
    skip-documentation: true
memory-usage:
    why: |
        The various memory components of an F5 unit are important to track to ensure a smooth operation. This includes the management plane's memory element (also called the linux host) as well as the data plane (TMM).
    how: |
        This script uses the F5 REST API to retrieve the current status of multiple different memory elements.
    without-indeni: |
        These memory metrics are available through SNMP, TMSH and the web user interface under Statistics/Performance.
    can-with-snmp: true
    can-with-syslog: false

#! REMOTE::HTTP
url: /mgmt/tm/sys/memory
protocol: HTTPS

#! PARSER::JSON

#Comments on VSX
#vCMP reserved memory for the guests and thus high memory usage is normal on the host.
#Thus resource metrics will be skipped for these.

# TMM Memory Used:
# This is the total amount of memory the system is using for traffic management.

# Other Memory Used:
# This represents the amount of memory the system is
# using for all processes other than Traffic Management Microkernel (TMM).

# Swap Used:
# This is the total amount of memory the system has reserved, but is not using, for all processes.

_metrics:
    -   #Collecting tmm free memory metric
        _groups:
            "$.entries.https://localhost/mgmt/tm/sys/memory/memory-host.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "memory-free-kbytes"
                    "im.dstype.displaytype":
                        _constant: "kilobytes"
                _temp:
                    "hostId":
                        _value: "*.nestedStats.entries.hostId.description"
                    "tmmMemoryFree":
                        _value: "*.nestedStats.entries.tmmMemoryFree.value"
        _transform:
            _tags:
                "name": |
                    {
                        print "host-id: " temp("hostId") " type: tmm"
                    }
            _value.double: |
                {
                    memory = temp("tmmMemoryFree") 
                    print memory/1000
                }
    -   #Collecting tmm total memory metric
        _groups:
            "$.entries.https://localhost/mgmt/tm/sys/memory/memory-host.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "memory-total-kbytes"
                    "im.dstype.displaytype":
                        _constant: "kilobytes"
                _temp:
                    "hostId":
                        _value: "*.nestedStats.entries.hostId.description"
                    "tmmMemoryTotal":
                        _value: "*.nestedStats.entries.tmmMemoryTotal.value"
        _transform:
            _tags:
                "name": |
                    {
                        print "host-id: " temp("hostId") " type: tmm"
                    }
            _value.double: |
                {
                    memory = temp("tmmMemoryTotal") 
                    print memory/1000
                }                
    -   #Collecting tmm memory usage
        _groups:
            "$.entries.https://localhost/mgmt/tm/sys/memory/memory-host.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "memory-usage"
                    "live-config":
                       _constant: "true"
                    "display-name":
                        _constant: "Memory - Usage"
                    "im.identity-tags":
                        _constant: "name"
                    "im.dstype.displaytype":
                        _constant: "percentage"
                    "resource-metric":
                        _constant: "true"
                _temp:
                    "hostId":
                        _value: "*.nestedStats.entries.hostId.description"
                    "tmmMemoryTotal":
                        _value: "*.nestedStats.entries.tmmMemoryTotal.value"
                    "tmmMemoryUsed":
                        _value: "*.nestedStats.entries.tmmMemoryUsed.value"
        _transform:
            _tags:
                "name": |
                    {
                        print "tmm usage - host-id: " temp("hostId")
                    }
            _value.double: |
                {
                    memoryTotal = temp("tmmMemoryTotal") 
                    memoryUsed = temp("tmmMemoryUsed") 
                    
                    print (memoryUsed/memoryTotal)*100
                }
    -   #Collecting host free memory metric
        _groups:
            "$.entries.https://localhost/mgmt/tm/sys/memory/memory-host.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "memory-free-kbytes"
                    "im.dstype.displaytype":
                        _constant: "kilobytes"
                _temp:
                    "hostId":
                        _value: "*.nestedStats.entries.hostId.description"
                    "memoryFree":
                        _value: "*.nestedStats.entries.memoryFree.value"
        _transform:
            _tags:
                "name": |
                    {
                        print "host-id: " temp("hostId") " type: total"
                    }
            _value.double: |
                {
                    memory = temp("memoryFree") 
                    print memory/1000
                }
    -   #Collecting host total memory metric
        _groups:
            "$.entries.https://localhost/mgmt/tm/sys/memory/memory-host.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "memory-total-kbytes"
                    "im.dstype.displaytype":
                        _constant: "kilobytes"
                _temp:
                    "hostId":
                        _value: "*.nestedStats.entries.hostId.description"
                    "memoryTotal":
                        _value: "*.nestedStats.entries.memoryTotal.value"
        _transform:
            _tags:
                "name": |
                    {
                        print "host-id: " temp("hostId") " type: total"
                    }
            _value.double: |
                {
                    memory = temp("memoryTotal") 
                    print memory/1000
                }                
    -   #Collecting host memory usage
        _groups:
            "$.entries.https://localhost/mgmt/tm/sys/memory/memory-host.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "memory-usage"
                    "live-config":
                       _constant: "true"
                    "display-name":
                        _constant: "Memory - Usage"
                    "im.identity-tags":
                        _constant: "name"
                    "im.dstype.displaytype":
                        _constant: "percentage"
                    "resource-metric":
                        _constant: "true"
                _temp:
                    "hostId":
                        _value: "*.nestedStats.entries.hostId.description"
                    "memoryTotal":
                        _value: "*.nestedStats.entries.memoryTotal.value"
                    "memoryUsed":
                        _value: "*.nestedStats.entries.memoryUsed.value"
        _transform:
            _tags:
                "name": |
                    {
                        print "total usage - host-id: " temp("hostId")
                    }
            _value.double: |
                {
                    memoryTotal = temp("memoryTotal") 
                    memoryUsed = temp("memoryUsed") 
                    
                    print (memoryUsed/memoryTotal)*100
                }          
    -   #Collecting host free memory metric for type other
        _groups:
            "$.entries.https://localhost/mgmt/tm/sys/memory/memory-host.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "memory-free-kbytes"
                    "im.dstype.displaytype":
                        _constant: "kilobytes"
                _temp:
                    "hostId":
                        _value: "*.nestedStats.entries.hostId.description"
                    "otherMemoryFree":
                        _value: "*.nestedStats.entries.otherMemoryFree.value"
        _transform:
            _tags:
                "name": |
                    {
                        print "host-id: " temp("hostId") " type: other"
                    }
            _value.double: |
                {
                    memory = temp("otherMemoryFree") 
                    print memory/1000
                }
    -   #Collecting host total memory metric for type other
        _groups:
            "$.entries.https://localhost/mgmt/tm/sys/memory/memory-host.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "memory-total-kbytes"
                    "im.dstype.displaytype":
                        _constant: "kilobytes"
                _temp:
                    "hostId":
                        _value: "*.nestedStats.entries.hostId.description"
                    "otherMemoryTotal":
                        _value: "*.nestedStats.entries.otherMemoryTotal.value"
        _transform:
            _tags:
                "name": |
                    {
                        print "host-id: " temp("hostId") " type: other"
                    }
            _value.double: |
                {
                    memory = temp("otherMemoryTotal") 
                    print memory/1000
                }                
    -   #Collecting host memory usage for type "other"
        _groups:
            "$.entries.https://localhost/mgmt/tm/sys/memory/memory-host.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "memory-usage"
                    "live-config":
                       _constant: "true"
                    "display-name":
                        _constant: "Memory - Usage"
                    "im.identity-tags":
                        _constant: "name"
                    "im.dstype.displaytype":
                        _constant: "percentage"
                    "resource-metric":
                        _constant: "true"
                _temp:
                    "hostId":
                        _value: "*.nestedStats.entries.hostId.description"
                    "otherMemoryTotal":
                        _value: "*.nestedStats.entries.otherMemoryTotal.value"
                    "otherMemoryUsed":
                        _value: "*.nestedStats.entries.otherMemoryUsed.value"
        _transform:
            _tags:
                "name": |
                    {
                        print "other usage - host-id: " temp("hostId")
                    }
            _value.double: |
                {
                    memoryTotal = temp("otherMemoryTotal") 
                    memoryUsed = temp("otherMemoryUsed") 
                    
                    print (memoryUsed/memoryTotal)*100
                }    
    -   #Collecting host free memory of type swap 
        _groups:
            "$.entries.https://localhost/mgmt/tm/sys/memory/memory-host.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "memory-free-kbytes"
                    "im.dstype.displaytype":
                        _constant: "kilobytes"
                _temp:
                    "hostId":
                        _value: "*.nestedStats.entries.hostId.description"
                    "swapFree":
                        _value: "*.nestedStats.entries.swapFree.value"
        _transform:
            _tags:
                "name": |
                    {
                        print "host-id: " temp("hostId") " type: swap"
                    }
            _value.double: |
                {
                    memory = temp("swapFree") 
                    print memory/1000
                }
    -   #Collecting host total memory of type swap
        _groups:
            "$.entries.https://localhost/mgmt/tm/sys/memory/memory-host.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "memory-total-kbytes"
                    "im.dstype.displaytype":
                        _constant: "kilobytes"
                _temp:
                    "hostId":
                        _value: "*.nestedStats.entries.hostId.description"
                    "swapTotal":
                        _value: "*.nestedStats.entries.swapTotal.value"
        _transform:
            _tags:
                "name": |
                    {
                        print "host-id: " temp("hostId") " type: swap"
                    }
            _value.double: |
                {
                    memory = temp("swapTotal") 
                    print memory/1000
                }                
    -   #Collecting host usage of memory type swap
        _groups:
            "$.entries.https://localhost/mgmt/tm/sys/memory/memory-host.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "memory-usage"
                    "live-config":
                       _constant: "true"
                    "display-name":
                        _constant: "Memory - Usage"
                    "im.identity-tags":
                        _constant: "name"
                    "im.dstype.displaytype":
                        _constant: "percentage"
                    "resource-metric":
                        _constant: "true"
                _temp:
                    "hostId":
                        _value: "*.nestedStats.entries.hostId.description"
                    "swapTotal":
                        _value: "*.nestedStats.entries.swapTotal.value"
                    "swapUsed":
                        _value: "*.nestedStats.entries.swapUsed.value"
        _transform:
            _tags:
                "name": |
                    {
                        print "swap usage - host-id: " temp("hostId")
                    }
            _value.double: |
                {
                    swapTotal = temp("swapTotal") 
                    swapUsed = temp("swapUsed") 
                    
                    print (swapUsed/swapTotal)*100
                }                
