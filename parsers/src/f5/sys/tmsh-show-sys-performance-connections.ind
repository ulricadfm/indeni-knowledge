 #! META
name: f5-tmsh-show-sys-performance-connections
description: Get system wide concurrent connection statistics
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: "f5"
    product: "load-balancer"
    linux-based: "true"
    shell: "bash"

#! COMMENTS
concurrent-connections:
    why: |
        Concurrent connections is important to track and graph for troubleshooting purposes. While F5 units do not have a static upper limit in terms of number of connections it is still is regulated by memory usage. 
    how: |
        This alert logs into the F5 unit through SSH and extracts the current number of concurrent connections via TMSH.
    without-indeni: |
        This metric is available by logging into the unit through SSH, entering TMSH and issuing the command "show sys performance connections".
    can-with-snmp: false
    can-with-syslog: false
#! REMOTE::SSH
tmsh -q show sys performance connections raw
 
#! PARSER::AWK

BEGIN{
    connsPerSec = 0
}

/^Connections/{
    writeDoubleMetric("concurrent-connections", null, "number", 60, $2)
}

/^(Client|Server) Connections/{
    connsPerSec += $3
}

END{
    writeDoubleMetric("connections-per-second", null, "number", 60, connsPerSec)
}
