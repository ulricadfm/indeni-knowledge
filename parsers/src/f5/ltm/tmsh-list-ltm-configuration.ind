#! META
name: f5-tmsh-list-ltm-configuration
description: Determine if there is any LTM configuration on a vCMP host
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    linux-based: "true"
    shell: "bash"
    vsx: "true"

#! COMMENTS
f5-vcmp-host-existing-ltm-configuration:
    why: |
        F5 does not recommend having LTM configurations on a vCMP host. A vCMP host is meant to serve as a dedicated hypervisor for its guests. Because of this, an engineer should expect to see nearly 100% of the vCMP host resources being utilized. Having an LTM configuration on the vCMP host itself (versus as a vCMP guest) can result in negative performance impact.
    how: |
        This alert logs into the F5 device via SSH, enters TMSH and by using the "list ltm" command, lists all configurations that have created listeners or are utilizing resources on the vCMP host.
    without-indeni: |
        Log into the device through SSH. Enter TMSH and issue the command "cd /;list ltm recursive one-line". Look through each entry in the list and look for configuration that would create listeners or use system resources.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
nice -15 echo -e "Node configuration: $(nice -15 tmsh -c 'cd /;list ltm node recursive one-line')"; nice -15 echo -e "Pool configuration: $(nice -15 tmsh -c 'cd /;list ltm pool recursive one-line')"; nice -15 echo -e "Virtual-server configuration: $(nice -15 tmsh -c 'cd /;list ltm virtual recursive one-line')"; nice -15 echo -e "Virtual-address configuration: $(nice -15 tmsh -c 'cd /;list ltm virtual-address recursive one-line')"; nice -15 echo -e "NAT configuration: $(nice -15 tmsh -c 'cd /;list ltm nat recursive one-line')"; nice -15 echo -e "SNAT configuration: $(nice -15 tmsh -c 'cd /;list ltm snat recursive one-line')";

#! PARSER::AWK

BEGIN {
    iLTMConf = 0
}

#Node configuration: ltm node Common/test { address 1.1.1.1 }
#Pool configuration:
#Virtual-server configuration:
#Virtual-address configuration:
#NAT configuration:
#SNAT configuration:
/^[a-zA-Z-]+?\sconfiguration:/{

    if(NF > 2){

        iLTMConf++
        configurationObjectArray[iLTMConf, "type"] = $1
    
    }

}

END {

    writeComplexMetricObjectArray("f5-vcmp-host-existing-ltm-configuration", null, configurationObjectArray)

}
