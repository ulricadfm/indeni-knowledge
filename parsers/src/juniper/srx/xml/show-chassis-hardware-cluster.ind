#! META
name: junos-show-chassis-hardware-node
description: Retrieve chassis cluster nodes serial number
type: monitoring
monitoring_interval: 59 minute
requires:
    vendor: juniper
    os.name: junos
    product: firewall
    high-availability: true

#! COMMENTS
serial-numbers:
    why: |
        Capture the device's serial number as well as important components' serial numbers. This makes inventory tracking easier.
    how: |
        This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show chassis hardware node X" command. The output includes the device's hardware and serial number details.
    without-indeni: |
        The administrator has to log on the system to run the "show chassis hardware node X" command to get the same information.
    can-with-snmp: true
    can-with-syslog: false

#! REMOTE::SSH
show chassis hardware node local | display xml

#! PARSER::XML
_vars:
    root: /rpc-reply

_dynamic_vars:
    _temp:
        "node":
            _text: ${root}//re-name
    _transform:
        _dynamic:
            "node": |
                {
                    if ( temp("node") == "node0" ) {
                        print "0"
                    } else {
                        print "1"
                    }
                }

#! REMOTE::SSH
show chassis hardware node ${node} | display xml

#! PARSER::XML
_vars:
    root: /rpc-reply
_metrics:
    -
        _groups:
            ${root}//chassis | ${root}//chassis-module:            
                _tags:
                    "im.name":
                        _constant: "serial-numbers"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Serial Numbers"
                _value.complex:
                    "name":
                        _text: "name"
                    "serial-number":
                        _text: "serial-number"
        _value: complex-array

