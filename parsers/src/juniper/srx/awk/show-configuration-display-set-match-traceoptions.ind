#! META
name: junos-show-configuration-display-set-match-traceoptions
description: JUNOS identify which component is enabled with traceoptions 
type: monitoring
monitoring_interval: 10 minute
requires:
    vendor: juniper
    os.name: junos
    product: firewall

#! COMMENTS
debug-status:
    why: |
        Traceoptions are enabled for debugging purpose to troubleshoot some issues. But the traceoptions also have negative impact on the device.
    how: |
        This script identifies which component is enabled with traceoptions by running the command "show configuration | display set | match traceoptions" via SSH connection to the device. 
    without-indeni: |
        An administrator could log on to the device to run the command "show configuration | display set | match traceoptions" to collect the same information.
    can-with-snmp: false 
    can-with-syslog: false
    vendor-provided-management: |
        The commamnd line is available to retrieve the same information

#! REMOTE::SSH
show configuration | display set | match traceoptions

#! PARSER::AWK

#set security ike traceoptions flag all
#deactivate security ike traceoptions
/^(set|deactivate)/{

    line = $0
    split(line, words, " traceoptions")
    if (words[1] ~ /deactivate/) {
           debug_status = 0
           gsub("deactivate ", "", words[1])
    } else {
           debug_status = 1
           gsub("set ", "", words[1])
    }
    if (debug_component[words[1]] != "0") {
          debug_component[words[1]] = debug_status 
    }
}

END{
     for (var in debug_component){
         debug_item["name"] = var
         debug_new = debug_component[var]
         writeDoubleMetric("debug-status", debug_item, "gauge", 60, debug_new)
     }
}
