#! META
name: chkp-ipso-show_sysenv
description: Show list of hardware status
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    os.name: ipso
    asg:
        neq: true

#! COMMENTS
hardware-element-status:
    why: |
        It is not uncommon for hardware components to fail inside a device without the device itself failing. In such an event they need to be replaced quickly.
    how: |
        Use clish "show sysenv all" command to list hardware health.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing hardware health is only available from the command line interface and WebUI.

#! REMOTE::SSH
stty rows 80 ; /usr/bin/nice -n 15 clish -c "show sysenv all"

#! PARSER::AWK

# The command returns a variety of tables, with a variety of columns. The script here will need to
# address them accordingly.

/(Location|ID)/ {
	delete columns
	getColumns(trim($0), "[ \t]+", columns)
}

# 1       SYS_FAN1  Normal  109            108           155  
# PS-A  Yes      n/a     n/a     OK      0  
# 1       SYSTEM    Good    40             75          1  
# 1       3.3V      Good    3.300    3.266     -0.034    3.096     3.487     
/[0-9]/ {
	name = getColData(trim($0), columns, "Location")
	if (name == "" || name == null) {
		name = getColData(trim($0), columns, "ID")   # The power supply status table is different
	}

	statusName = getColData(trim($0), columns, "Status")

	if (statusName == "Good" || statusName == "OK" || statusName == "Normal") {
		status = 1
	} else {
		status = 0
	}
	hwTags["name"] = name
	writeDoubleMetricWithLiveConfig("hardware-element-status", hwTags, "gauge", 300, status, "Hardware Status", "state", "name")
}
