#! META
name: chkp-mgmt-mdsstat-mds
description: Monitor CMA processes
type: monitoring
monitoring_interval: 1 minutes
requires:
    vendor: checkpoint
    os.name: gaia
    role-management: true
    mds: true

#! COMMENTS
process-state:
    skip-documentation: true

#! REMOTE::SSH
COLUMNS=150 && export COLUMNS && ${nice-path} -n 15 mdsstat

#! PARSER::AWK

# Reads the status of the critical processes for each CMA. This will not be covered with using only "cpwd_admin list" since if stopping a CMA the process for that will be removed from that list.

BEGIN {
	# Input is divided on pipe
	FS = "|"
}




#| CMA |lab-CP-MGMT-MDM-VS1_Management_Server                                         | 192.168.197.34  | up 5814    | up 5739  | up 5546  | up 8450  |
#| MDS |                                       -                                      | 192.168.197.33  | down       | up 5840  | up 5837  | up 8375  |
/^\|\s+(MDS|CMA)/ {
	# Remove old tags
	delete tags

	vsName = trim($3)
	vsIp = trim($4)

	fwm = $5
	fwd = $6
	cpd = $7
	cpca = $8

	# Set VS tags if this is the CMA, but do not set them if this is the MDS
	if (trim($2) != "MDS") {
		tags["vs.ip"] = vsIp
		tags["vs.name"] = vsName
	}

	# FWM
	if (fwm ~ "up") {
		fwmStatus = 1
	} else {
		fwmStatus = 0
	}
	tags["description"] = "FWM"
	writeDoubleMetric("process-state", tags, "gauge", 60, fwmStatus)

	# FWD
	if (fwd ~ "up") {
		fwdStatus = 1
	} else {
		fwdStatus = 0
	}
	tags["description"] = "FWD"
	writeDoubleMetric("process-state", tags, "gauge", 60, fwdStatus)

	# CPD
	if (cpd ~ "up") {
		cpdStatus = 1
	} else {
		cpdStatus = 0
	}
	tags["description"] = "CPD"
	writeDoubleMetric("process-state", tags, "gauge", 60, cpdStatus)

	# CPCA
	if (cpca ~ "up") {
		cpcaStatus = 1
	} else {
		cpcaStatus = 0
	}
	tags["description"] = "CPCA"
	writeDoubleMetric("process-state", tags, "gauge", 60, cpcaStatus)
}