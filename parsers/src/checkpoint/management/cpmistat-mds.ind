#! META
name: chkp-mgmt-cpmistat-mds
description: Show redundant mgmt sync status for all CMAs
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    os.name: gaia
    role-management: true
    mds: true

#! COMMENTS
mgmt-ha-sync-state:
    skip-documentation: true

mgmt-ha-sync-state-description:
    skip-documentation: true    

#! REMOTE::SSH
COLUMNS=150 && export COLUMNS && ${nice-path} -n 15 mdsstat |grep CMA | awk '{gsub(/\|/,"",$3); print $3}' | while read name; do mdsenv $name  && (${nice-path} -n 15 mdsstat $name && ${nice-path} -n 15 cat $FWDIR/database/netobj_objects.C) | ${nice-path} -n 15 awk '/^\| CMA \|/ { vsName=$3; vsIp=$5; gsub(/\|/,"",vsName) } /^\t: \(.+$/ { if (host && mg == 2 ) { gsub(/\t\: \(/,"",host); print  "cpmistat -o schema -r mg " host " vsIP: " vsIp " vsName: " vsName } mg = 0; host=$0 } /:primary_management \(false\)/ { mg++ } /:management \(true\)/ { mg++ } END { if (host && mg == 2 ) { gsub(/\t\: \(/,"",host); print  "cpmistat -o schema -r mg " host " vsIP: " vsIp " vsName: " vsName} }' ; done | (while read cmd ; do { echo "1 cmd is $cmd"; cmd_command=$(awk -v var="$cmd" 'BEGIN { split(var,splitArr," "); print splitArr[1] " " splitArr[2] " " splitArr[3] " " splitArr[4] " " splitArr[5] " " splitArr[6]};'); echo "cmd-command is $cmd_command"; mdsName=$(awk -v var="$cmd" 'BEGIN { split(var,splitArr," "); print splitArr[10];}'); mdsIP=$(awk -v var="$cmd" 'BEGIN { split(var,splitArr," "); print splitArr[8];}'); mdsenv $mdsName; ($cmd_command && echo $cmd) & sleep 0.1 ; pid=$(ps aux|grep "$cmd_command" |grep -v "grep" |awk '{print $2}') ;  PID_LIST+=" $pid"; echo "Process $cmd_command started for MDS: $mdsName IP: $mdsIP with pid: $pid"; } done ; echo "I will sleep for 20s now" ; sleep 20; for id in $PID_LIST ; do echo "I will kill $id"; kill $id; done) ; sleep 1; echo;
   
#! PARSER::AWK

############
# Script explanation: This script is a bit complicated. Here is how it works
# Step 1: List all CMA
# Step 2: For each CMA, list all secondary management servers by:
#		Parse netobj_objects.C and find all hosts that have the following two properties:
#		:primary_management (false)
#		:management (true)
# Step 3: For each of the devices found in step 2, run cpmistat command against them.
# To be able to query as many devices as possible, and have everything ready within the 30s ind script timeout, run cpmistat in parallel (since it has a 35s timeout), and record the PID of each command so we can kill them all after 20s.
###########

function addVsTags(tags) {
	tags["vs.ip"] = vsIp
    tags["vs.name"] = vsName
}


# Process cpmistat -o schema -r mg lab-CP-MGMT-R7730-HA started for MDS: lab-CP-MGMT-R7730_Management_Server IP: 10.10.6.10 with pid: 28708
/^Process \"cpmistat -o schema -r mg/ {
	# We have attempted to connect to this host. If we do not get a valid response we need to mark this one as fail.
	hostname = $7
	vsName = $11
	vsIp = $13
	status = 0
	statusMessage = "nomessage"
	
	# Hostname can be the same for several CMAs, combining with VSNAME to have unique index
	iIndex = hostname ";" vsName
	statusArr[iIndex] = vsIp ";" status ";" statusMessage
}

# :mgStatusOK (0)
# :mgStatusOK (1)
/:mgStatusOK/ {
	# We got some result from the attempt, but we dont know from who
	status = $2
	gsub(/^\(/,"",status)
	gsub(/\)$/,"",status)
}

# :mgSyncStatus (Lagging)
# :mgSyncStatus (Synchronized)
# :mgSyncStatus ("N/R (Self synchronization is not relevant)")
/:mgSyncStatus/ {
	# We got some result from the attempt, but we dont know from who
	# We will store the message in the meantime
	split($0,splitArr,"mgSyncStatus")
	statusMessage = trim(splitArr[2])
	gsub(/^\(/,"",statusMessage)
	gsub(/\)$/,"",statusMessage)
	gsub(/\"/,"",statusMessage)
}


# cpmistat -o schema -r mg lab-CP-MGMT-R7730-HA vsIP: 10.10.6.10 vsName: lab-CP-MGMT-R7730_Management_Server
/^cpmistat -o schema -r mg/ {
	# We now know from who the above result was from
	hostname = $6
	vsName = $10
	vsIp = $8
	
	iIndex = hostname ";" vsName
	statusArr[iIndex] = vsIp ";" status ";" statusMessage

	# Reset variables
	statusMessage = ""
	status = ""
}


END {
	for (id in statusArr) {
		split(statusArr[id],splitDataArr,";")
		split(id,splitIdArr,";")
		hostname = splitIdArr[1]
		vsName = splitIdArr[2]
		vsIp = splitDataArr[1]
		status = splitDataArr[2]
		statusMessage = splitDataArr[3]
		if (status < 2) {
			addVsTags(t)
			t["name"] = hostname
			writeDoubleMetric("mgmt-ha-sync-state", t, "gauge", 300, status)
			writeComplexMetricString("mgmt-ha-sync-state-description", t, statusMessage)
		}
	}
}
