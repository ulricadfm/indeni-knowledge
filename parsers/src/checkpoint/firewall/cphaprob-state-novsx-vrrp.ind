#! META
name: chkp-cphaprob_state_monitoring-vrrp
description: Run "cphaprob state" for cluster status monitoring for VRRP
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: "checkpoint"
    high-availability: "true"
    vrrp: "true"

#! COMMENTS
cluster-mode:
    skip-documentation: true

cluster-member-active:
    skip-documentation: true

cluster-member-states:
    skip-documentation: true

cluster-state:
    skip-documentation: true

#! REMOTE::SSH
stty rows 80 ; ${nice-path} -n 15 clish -c "show vrrp" && ${nice-path} -n 15 cphaprob state

#! PARSER::AWK

############
# Why: Important to know the status of the cluster
# How: Use both cphaprob state and show vrrp to get the complete picture

## This script is for VRRP clusters

# A remote peer can have the following states
# Down
# Active

# A local peer can have the following states
# Down
# Ready
# Active
# HA module not started.

### The cluster as a whole can have several states regarding different aspects of the cluster
## Redundancy
# The non-active member could be:
# Ready - This member will take over if the Active one goes down, but it is not syncing state tables, so the failover will cut all connections
# Down - This member will not take over in case the active member goes offline

## Health
# Active - This member is healthy and forwarding traffic

## VRRP
# A VRRP cluster shows both members as "Active" in cphaprob state. This is normal.
###########


BEGIN {
	foundActive = "false"
	foundUnhealthyState = "false"
	tags["name"] = "VRRP"
}

# Find out the real status on the local member, since cphaprob state always shows "Active" even when Standby
#        In Backup state 2
#        In Master state 0
/\s+(In Master state|In Backup state)/{
 
	if ($2 == "Backup" && $4 > 0) {
		remoteRole = "Active"
	} else {
		remoteRole = "Standby"
	}

	if ($2 == "Master" && $4 > 0){
		localRole = "Active"
	} else {
		localRole = "Standby"
	}
}

# Determine cluster mode
#Cluster Mode:   Sync only (OPSEC) with IGMP Membership
/^Cluster Mode\:\s+Sync only/ {
	clustermode = "Sync only"
	
	writeComplexMetricString("cluster-mode", tags, clustermode)
}

# Has the cluster at least one healthy member forwarding traffic?
# Match any cluster member being Active (but not Active Attention)
#1 (local)  10.10.6.21      Active
/^\d+\s.*Active$/ {
		foundActive = "true"
}

# The state of the local cluster member
#1 (local)  10.10.6.21      Active
/(local)/ {

	# Catch both "Active" and "Active Attention"
	if ($0 ~ /Active/) {
		if (localRole == "Active") {
			localState = 1
		} else {
			localState = 0
		}
	} else {
		localState = 0
	}

	writeDoubleMetricWithLiveConfig("cluster-member-active", tags, "gauge", "60", localState, "Cluster Member State (this)", "state", "cluster-member-state-description")
}




# Record all cluster members states into array
# Match any cluster state lines
#1 (local)  10.10.6.21      Active
#2          10.10.6.22      Down
/^\d+\s.*\d+\.\d+\.\d+\.\d+\s+/ {

	iAllMembers++
	state = $NF
	uniqueIp = $(NF-1)

	
	allMembers[iAllMembers, "state-description"] = state
	allMembers[iAllMembers, "id"] = $1
	allMembers[iAllMembers, "unique-ip"] = uniqueIp
	
	if ( $2 == "\(local\)") {
		allMembers[iAllMembers, "is-local"] = 1
		# If state is "Active" then we need to determine the real status, Active or Standby
		if (state == "Active" && localRole) {	
			allMembers[iAllMembers, "state-description"] = localRole
		}
	} else {
		allMembers[iAllMembers, "is-local"] = 0	
		# If state is "Active" then we need to determine the real status, Active or Standby
		if (state == "Active" && remoteRole) {	
			allMembers[iAllMembers, "state-description"] = remoteRole
		}
	}
}

END {

	# Not good if both members have active interfaces
	if (localRole == "Active" && remoteRole == "Active") {
		bothActive = "true"
	}
	
	# If at least one member forwarding traffic is found it is ok. If the cluster services are not running the state is unclear and an alert is issued.
	clusterstate = 0
	if (foundActive == "true" && bothActive != "true") {
		clusterstate = 1
	}
	writeDoubleMetricWithLiveConfig("cluster-state", tags, "gauge", "60", clusterstate, "Cluster State", "state", "name")

	# Write status of all members
	if (iAllMembers != "") {
		writeComplexMetricObjectArray("cluster-member-states", tags, allMembers)
	}
}