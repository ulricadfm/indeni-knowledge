#! META
name: chkp-cphaprob_state_monitoring-vsx
description: Run "cphaprob state" for cluster status monitoring on VSX
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: "checkpoint"
    high-availability: "true"
    vsx: "true"
    clusterxl: "true"
    role-firewall: "true"
    asg:
        neq: "true"

#! COMMENTS
cluster-mode:
    skip-documentation: true

cluster-member-active:
    skip-documentation: true

cluster-member-states:
    skip-documentation: true

cluster-state:
    skip-documentation: true

#! REMOTE::SSH
${nice-path} -n 15 fw vsx stat -l | grep VSID | awk '{print $NF}' | while read id; do vsenv $id && fw vsx stat $id && ${nice-path} -n 15 cphaprob state ; done

#! PARSER::AWK

############
# Why: It is importat to know the status of the cluster.
# How: Use "cphaprob state"

# A remote peer can have the following states
# Down
# Ready
# Active
# Active Attention
# ClusterXL Inactive or Machine is Down

# A local peer can have the following states
# Down
# Ready
# Active
# Active Attention
# HA module not started.

### The cluster as a whole can have several states regarding different aspects of the cluster
## Redundancy
# The non-active member could be:
# Standby - This is the best state for a non-active member
# Ready - This member will take over if the Active one goes down, but it is not syncing state tables, so the failover will cut all connections
# Down - This member will not take over in case the active member goes offline

## Health
# Active - This member is healthy and forwarding traffic
# Active Attention - This member is not healthy and would like to fail over, but there is no other node that can take over.
###########

function dumpClusterStateData() {
	tags["vs.id"] = vsId
	tags["vs.name"] = vsName
	# If we found at least one member forwarding traffic, and no cluster members in an unhealthy state then its ok
	clusterState = 0
	if (foundActive == "true" && foundUnhealthyState=="false") {
		clusterState = 1
	}

	# Write status of all members
	if (iAllMembers != "") {
		writeComplexMetricObjectArray("cluster-member-states", tags, allMembers)
	}

	if ( vsType != "Switch") {
		writeDoubleMetricWithLiveConfig("cluster-state", tags, "gauge", "60", clusterState, "Cluster State", "state", "name")
	}
}


BEGIN {
	foundActive = "false"
	foundUnhealthyState = "false"
	vsId = ""
	vsName = ""
	tags["name"] = "ClusterXL"
}

#VSID:            0
/^VSID:\s+[0-9]/ {
	# Dump data of previous VS if needed
	if (vsId != "") {
		dumpClusterStateData()
	}
	vsId = $NF

	# Delete the old
	delete tags
	state = ""
	delete allMembers
	foundActive = "false"
	status = ""
	statusFlag = ""
	foundUnhealthy = "false"
}

#Name:            lab-CP-VSX1-R7730
/^Name:\s+/ {
	vsName = trim($NF)
}

#Type:            VSX Gateway
#Type:            Virtual Switch
/^Type:\s+/ {
	vsType = trim($NF)
}

# Determine cluster mode
#Cluster Mode:   VSX High Availability (Active Up) with IGMP Membership
#Cluster Mode:   Virtual System Load Sharing
/^Cluster Mode:\s+/ {
	clusterMode = trim(substr($0, index($0, ":") + 1))
	tags["vs.id"] = vsId
	tags["vs.name"] = vsName
	writeComplexMetricString("cluster-mode", tags, clusterMode)
}

# Has the cluster at least one healthy member forwarding traffic?
# Match any cluster member being Active (but not Active Attention)
#1 (local)  10.11.2.21      0%              Active
/^\d+\s.*Active$/ {
	foundActive = "true"
}

# The state of the local cluster member
#1 (local)  10.11.2.21      0%              Standby
/[0-9]+ \(local\)/ {
	# Catch both "Active" and "Active Attention"
	if ($0 ~ /Active/) {
		localState = 1
	} else {
		localState = 0
	}
	
	tags["vs.id"] = vsId
	tags["vs.name"] = vsName
	writeDoubleMetricWithLiveConfig("cluster-member-active", tags, "gauge", "60", localState, "Cluster Member State (this)", "state", "cluster-member-state-description")
}

# Match local cluster member not having cluster services started
#HA module not started.
/^HA module not started./ {
	foundUnhealthyState = "true"
}


# Record all cluster members states into array
# Match any cluster state lines
#1 (local)  10.11.2.21      0%              Active
/^\d+\s.*\d+\.\d+\.\d+\.\d+\s+/ {

	iAllMembers++
	state = $NF
	assignedLoad = $(NF-1)
	uniqueIp = $(NF-2)
	
	if ($0 ~ /Active Attention/) {
		state = "Active Attention"
		assignedLoad = $(NF-2)
		uniqueIp = $(NF-3)
	}
	
	if ($0 ~ /ClusterXL Inactive/) {
		state = "ClusterXL Inactive or Machine is Down"
		assignedLoad = "0%"
		uniqueIp = $(NF-7)
	}
	
	allMembers[iAllMembers, "state-description"] = state
	allMembers[iAllMembers, "id"] = $1
	allMembers[iAllMembers, "assigned-load-percentage"] = assignedLoad
	allMembers[iAllMembers, "unique-ip"] = uniqueIp
	
	if ( $2 == "\(local\)") {
		allMembers[iAllMembers, "is-local"] = 1
	} else {
		allMembers[iAllMembers, "is-local"] = 0	
	}
}

END {

	#dumpClusterStateData()
	# Dump data of last VS
	if (vsId != "") {
		dumpClusterStateData()
	}
}
