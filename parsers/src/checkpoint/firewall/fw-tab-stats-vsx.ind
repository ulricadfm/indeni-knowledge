#! META
name: fw_tab_stats_vsx
description: run "fw tab" on all vs's in VSX
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: checkpoint
    vsx: "true"
    role-firewall: true

#! COMMENTS
kernel-table-actual:
    skip-documentation: true
	
kernel-table-limit:
    skip-documentation: true

#! REMOTE::SSH
${nice-path} -n 15 fw vsx stat -l | grep VSID | awk '{print $NF}' | while read id; do vsenv $id && ${nice-path} -n 15 fw vsx stat $id && (${nice-path} -n 15 fw tab | grep -B 1 " limit " && ${nice-path} -n 15 fw tab -s); done

#! PARSER::AWK

function addVsTags(tags) {
	tags["vs.id"] = vsid
    tags["vs.name"] = vsname
}

function dumpVsTableData() {
    for (tablename in actuals) {
        tabletags["name"] = tablename
        addVsTags(tabletags)

        writeDoubleMetric("kernel-table-actual", tabletags, "gauge", "60", actuals[tablename])
        if (limits[tablename] != "") {
            writeDoubleMetric("kernel-table-limit", tabletags, "gauge", "60", limits[tablename])
        }
        # Clear the tags
        split("", tabletags)
    }
}

BEGIN {
	vsid=""
	vsname=""
}

# VSID:            0
/VSID:/ {
    if (vsid != "") {
        # write the previous VS's data
        dumpVsTableData()
    }

    # New VSID, zero out the arrays
    split("", limits)
    split("", actuals)

	vsid = trim($NF)
}

# Name:            VSX-CXL2-Gear
/Name:/ {
	vsname = trim($NF)
}

# -------- drop_tmpl_timer --------
/^----/ {
	if (NF == 3) {
	    tablename = $2
	}
}

# # dynamic, id 142, attributes: expires 3600, refresh, , hashsize 512, limit 1
/dynamic.*limit \d/ {
    if (match($0, "limit \\d+")) {
        limit = substr($0, RSTART+6, RSTART + RLENGTH - 6)
        limits[tablename] = limit
    }
}

# localhost             vsx_firewalled                       0     1     1       0
/localhost.*/ {
    tablename = $2
    value = $6
    actuals[tablename] = value
}

END {
    # The last VS is handled here:
    dumpVsTableData()
}
