#! META
name: cplic-print-x-n-p
description: get full license data via cplic
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: checkpoint
    mds:
        neq: true

#! COMMENTS
licenses:
    why: |
        License features should not differ between members of the same cluster. If they do, a fail over to the secondary member may mean some functionality is not operating the way it is expected to.
    how: |
        By using the Check Point built-in "cplic print" command, the features are retrieved.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Comparing licenses in a cluster can be done through Check Point SmartUpdate, manually.

#! REMOTE::SSH
${nice-path} -n 15 cplic print -x -n -p

#! PARSER::AWK

# 192.168.250.5    never       aMnLnwjT6HedbJTAehzPVrt3f7YejRc2iQij  ::CK-00-1C-7F-3E-CB-38 fw1:6.0:swb evnt:6.0:smrt_evnt fw1:6.0:fwc fw1:6.0:ca fw1:6.0:rtmui fw1:6.0:sstui fw1:6.0:fwlv fw1:6.0:cmd evnt:6.0:alzd5 evnt:6.0:alzc1 evnt:6.0:alzs1 fw1:6.0:fwc fw1:6.0:ca fw1:6.0:rtmui fw1:6.0:sstui fw1:6.0:fwlv fw1:6.0:cmd evnt:6.0:alzd5 evnt:6.0:alzc1 evnt:6.0:alzs1 fw1:6.0:swb fw1:6.0:cluster-1 fw1:6.0:swb fw1:6.0:cpxmgmt_qos_u_sites fw1:6.0:sprounl
/fw1:/ {
    ilicense++

    licenses[ilicense, "ip"]=$1
    licenses[ilicense, "expiration"]=$2
    licenses[ilicense, "signature"]=$3
    licenses[ilicense, "ck"]=$4
    features=$4
    for (i = 5; i<=NF; i++) {
        ifeature++
        features_arr[ifeature, "name"] = $i
        features=sprintf("%s %s", features, $i)
    }
    licenses[ilicense, "features"] = features
}

END {
	writeComplexMetricObjectArray("licenses", null, licenses)
}
