#! META
name: chkp-stat-size-fw-log
description: Get the file size (in bytes) of fw.log
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    role-firewall: true
    vsx:
        neq: true
    os.name:
        neq: gaia-embedded   # intentional per IKP-932

#! COMMENTS
log-file-size:
    why: |
        In case the device has lost connectivity to its log server, it will begin to log all traffic log locally. If this is not discovered, then logs can be missing from the central log repository or the storage may fill up.
    how: |
        By repeatedly retreiving the file size of the fw.log file, any increase in size can be alerted upon.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Tracking the size of the log file is only possible from the command line interface.

#! REMOTE::SSH
${nice-path} -n 15 stat -c "%s" $FWDIR/log/fw.log

#! PARSER::AWK

#18428
/^[0-9]+/ {
	tags["path"] = "$FWDIR/log/fw.log"
    writeDoubleMetricWithLiveConfig("log-file-size", tags, "counter", "300", $1, "File Sizes - Log File", "number", "")
}
