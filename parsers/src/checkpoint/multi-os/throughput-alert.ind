#! META
name: chkp-os-throughput-alert
description: Check the current throughput for recieve and transmit for interfaces.
type: monitoring
monitoring_interval: 1 minutes
requires:
    vendor: checkpoint
    or:
        -
            os.name: gaia
        -
            os.name: secureplatform
        # os.name: gaia-embedded removed per   IKP-932

#! COMMENTS
network-interface-tx-util-percentage:
    why: |
        If the throughput reaches the limit, packets will be dropped.
    how: |
        Indeni will record the total bytes transmitted, wait a pre-determined amount of time and then record it again. By comparing before and after a value for how many bytes sent during the period of time can be determined.
    without-indeni: |
        An administrator could login and manually check this from the command line interface.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface or SmartView Monitor.

network-interface-rx-util-percentage:
    why: |
        If the throughput reaches the limit, packets will be dropped.
    how: |
        Indeni will record the total bytes received, wait a pre-determined amount of time and then record it again. By comparing before and after a value for how many bytes sent during the period of time can be determined.
    without-indeni: |
        An administrator could login and manually check this from the command line interface.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface or SmartView Monitor.

#! REMOTE::SSH
${nice-path} -n 15 ifconfig -a|grep "HWaddr"| awk {'print $1'}| while read interface; do ${nice-path} -n 15 ethtool $interface; done && ${nice-path} -n 15 ifconfig && sleep 10 && ${nice-path} -n 15 ifconfig

#! PARSER::AWK

##################################
#
#	Determine interface name
#	
##################################

#LAN1.246      Link encap:Ethernet  HWaddr 00:1C:7F:23:26:EB
/Link encap/ {
    interfaceName = $1
}

#Settings for eth0:
/^Settings for / {
	interfaceName = $3
	gsub(/:/, "", interfaceName)
}

##################################
#
#	Record bytes sent/received
#	
##################################

#RX bytes:3964467449 (3.6 GiB)  TX bytes:922468769 (879.7 MiB)
/X bytes/ {
	bytesRx = $2
	bytesTx = $6
	
	# Remove "bytes:"
	gsub(/bytes:/, "", bytesRx)
	gsub(/bytes:/, "", bytesTx)

	
	if (interfaceName in bytesTxArr) { # if the array bytesTxArr already contains data for the interface
		# Calculate diff
		# How many bytes sent first run - how many bytes sent second run
		# Divided by 10 - To get into per second as first and second run was 10 seconds apart
		# Times 8 - convert from byte to bit
		# Divided into 1000 - Get into kilobit
		bytesTxArr[interfaceName] = (((bytesTx - bytesTxArr[interfaceName]) / 10) * 8) / 1000  # Result kilobit per second
	} else {
		bytesTxArr[interfaceName] = bytesTx
	}
	
	if (interfaceName in bytesRxArr) { # if the array bytesRxArr already contains data for the interface
		# Calculate diff
		# How many bytes sent first run - how many bytes sent second run
		# Divided by 10 - To get into per second as first and second run was 10 seconds apart
		# Times 8 - convert from byte to bit
		# Divided into 1000 - Get into kilobit
		bytesRxArr[interfaceName] = (((bytesRx - bytesRxArr[interfaceName]) / 10) * 8) / 1000 # Result kilobit per second
	} else {
		bytesRxArr[interfaceName] = bytesRx
	}
}


##################################
#
#	Determine speed of interface
#	
##################################

#        Speed: 1000Mb/s
/\s+Speed: / {
	speedUnit = $2
	speedData = $2
	
	# Remove digits and b/s, keeping only unit prefix
	gsub(/[0-9]+|b\/s/, "", speedUnit)
	
	# Remove prefix and b/s
	gsub(/[A-Za-z]+|\/s/, "", speedData)
	
	# Have not seen prefix on interface speed be anything else than Mega so far, but this would make it easy to add others if needed in the future.
	if (speedUnit == "M") {
		speedKbitArr[interfaceName] = speedData * 1000
	}
}

END {

	##################################
	#
	#	Remove interfaces that do not 
	#	have a known speed
	#	
	##################################
	for (interface in bytesTxArr) {
		if (!(interface in speedKbitArr)) {
			delete bytesTxArr[interface]
		}	
	}
	
	for (interface in bytesRxArr) {
		if (!(interface in speedKbitArr)) {
			delete bytesRxArr[interface]
		}	
	}
	
	##################################
	#
	#	Calculate percentage interface 
	#	usage and write metric data
	#	
	##################################	

	for (interface in bytesTxArr) {
		interfaceTags["name"] = interface
		percentageTxUsed[interface] = (bytesTxArr[interface] / speedKbitArr[interface]) * 100
		writeDoubleMetricWithLiveConfig("network-interface-tx-util-percentage", interfaceTags, "gauge", "60", percentageTxUsed[interface], "Network Interfaces - Throughput Transmit", "percentage", "name")
	}
	
	for (interface in bytesRxArr) {
		interfaceTags["name"] = interface
		percentageRxUsed[interface] = (bytesRxArr[interface] / speedKbitArr[interface]) * 100
		writeDoubleMetricWithLiveConfig("network-interface-rx-util-percentage", interfaceTags, "gauge", "60", percentageRxUsed[interface], "Network Interfaces - Throughput Receive", "percentage", "name")
	}
}