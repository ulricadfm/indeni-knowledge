#! META
name: chkp-clish-show_aaa_radius_servers_list
description: run "show aaa radius-servers list" over clish
type: monitoring
monitoring_interval: 10 minutes
requires:
    vendor: checkpoint
    or:
        -
            os.name: gaia
        -
            os.name: ipso

#! COMMENTS
radius-servers:
    why: |
        If the RADIUS servers are configured incorrectly, it might not be possible for an administrator to login to the device.
    how: |
        Parse the gaia configuration database in /config/active and retreive the currently configured RADIUS servers. It is also possible to list them using clish, but that generates a large amount of logs in /var/log/messages when done repeatedly.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing RADIUS servers is only available from the command line interface and WebUI.

radius-super-user-id:
    why: |
        The RADIUS super user ID is the UID the user has when entering expert mode. If this is not 0 (root) and instead the default of 96, then the user will not have permission to access some file and tools.
    how: |
        indeni parses the gaia configuration database in /config/active and retreive the currently configured RADIUS super user id. It is also possible to list them using clish, but that generates a large amount of logs in /var/log/messages when done repeatedly.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing the RADIUS configuration is only available from the command line interface and WebUI.

#! REMOTE::SSH
${nice-path} -n 15 grep "aaa:auth_profile:base_radius_authprofile" /config/active

#! PARSER::AWK

# aaa:auth_profile:base_radius_authprofile:radius_srv:1:timeout 4
/aaa:auth_profile:base_radius_authprofile:radius_srv:[0-9]+:timeout/ {
	split($1, timeoutSplitArr, ":")
	priority = timeoutSplitArr[5]

	servers[priority, "priority"] = priority
	servers[priority, "timeout"] = $NF
}

# aaa:auth_profile:base_radius_authprofile:radius_srv:1:host 2.2.2.2
/aaa:auth_profile:base_radius_authprofile:radius_srv:[0-9]+:host/ {
	split($1, hostSplitArr, ":")
	priority = hostSplitArr[5]
	servers[priority, "host"] = $NF
}

# aaa:auth_profile:base_radius_authprofile:radius_srv:1:port 1812
/aaa:auth_profile:base_radius_authprofile:radius_srv:[0-9]+:port/ {
	split($1, portSplitArr, ":")
	priority = portSplitArr[5]
	servers[priority, "port"] = $NF
}

# aaa:auth_profile:base_radius_authprofile:radius_srv:super-user-uid 96
/aaa:auth_profile:base_radius_authprofile:radius_srv:super-user-uid/ {
	if (arraylen(servers)) {
		writeComplexMetricString("radius-super-user-id", null, $NF)
	}
}

END {
	writeComplexMetricObjectArray("radius-servers", null, servers)
}