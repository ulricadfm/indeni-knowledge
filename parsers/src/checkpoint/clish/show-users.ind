#! META
name: chkp-clish-show_users
description: run "show users" over clish
type: monitoring
monitoring_interval: 10 minutes
requires:
    vendor: checkpoint
    or:
        -
            os.name: gaia
        -
            os.name: ipso

#! COMMENTS
users:
    why: |
        Often user accounts are left enabled after administrators leave. Therefore it's important to have an easy way to review all accounts currently active.
    how: |
        Parse the Gaia/IPSO configuration database in /config/active and retreive the currently configured users. It is also possible to list them using clish, but that generates a large amount of logs in /var/log/messages when done repeatedly.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing local users is only available from the command line interface and WebUI.

#! REMOTE::SSH
${nice-path} -n 15 grep "mrma:users" /config/active

#! PARSER::AWK

BEGIN {
	# Lines are separated by ":"
	FS=":"
}

# mrma:users:user:indeni t
/mrma:users:user:/ {
	# Get only lines with 4 columns, the others are not relevant or duplicates
    if (NF == 4) {
		iuser++
		user=$4
		gsub(/ t/,"",user)
		users[iuser, "username"]=user
	}
}

END {
	writeComplexMetricObjectArray("users", null, users)
}
