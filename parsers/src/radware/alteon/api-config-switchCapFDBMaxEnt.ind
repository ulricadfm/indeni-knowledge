#! META
name: radware-api-config-switchCapFDBMaxEnt
description: fetch the limit of the FDB table
type: monitoring
monitoring_interval: 59 minute 
requires:
    os.name: "alteon-os"
    vendor: "radware"

#! COMMENTS
fdb-limit:
    why: |
        The FDB table fills up pretty quickly under high traffic loads. It is very common to run into FDB limits, but the limit fluctuates based on the form factor. For example, a vADC can potentially have a lower limit depending on what ports and vlans are assigned to it. An admin will need to know if the table is reaching capacity to determine if L2 forwarding will become an issue for the Alteon.
    how: |
        This script runs the "/config/switchCapFDBMaxEnt" through the Alteon API gateway.
    without-indeni: |
        An administrator would need to log in to the device and run a CLI command or run the API command "/config/switchCapFDBMaxEnt".
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        Can be done through Management GUI (Vision or Alteon VX).

#! REMOTE::HTTP
url: /config/switchCapFDBMaxEnt
protocol: HTTPS

#! PARSER::JSON
_metrics:
    -
        _value.double:
            _value: switchCapFDBMaxEnt
        _tags:
            "im.name":
                _constant: "fdb-limit"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "FDB Table - Limit"
            "im.dstype.displayType":
                _constant: "number"
