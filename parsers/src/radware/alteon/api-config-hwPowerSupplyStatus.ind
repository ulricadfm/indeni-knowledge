#! META
name: radware-api-config-hwPowerSupplyStatus
description: Get the status of the power supplies.
type: monitoring
monitoring_interval: 5 minute 
requires:
    os.name: "alteon-os"
    vendor: "radware"

#! COMMENTS
power-supply-inserted:
    why: |
        Network devices that do not have redundant power supplies are at risk for a complete service outage should there be a loss of power to the device. It is best practice to install multiple PSU's with separate power sources whenever possible. 
    how: |
        This script leverages the Radware Alteon's API to detect that all available PSU slots are in use. Detection of an empty PSU slot will trigger an alert. 
    without-indeni: |
        An administrator would need to SSH into CLI on the device and run the command "info/sys/ps" to clearly identify if a PSU is installed in each available slot. 
    can-with-snmp: true
    can-with-syslog: false
hardware-element-status:
    why: |
        It is difficult to identify when a power supply fails unless actively tragging the logs or setting up an SNMP trap. Even then, it is hard to determine whether or not the device comes back up within a reasonable amount of time and identifying patterns in which the PS fails is important as well.
    how: |
        This script leverages the Radware Alteon's API to detect that all available PSU slots are in use. Detection of an empty PSU slot will trigger an alert. 
    without-indeni: |
        An administrator would need to SSH into CLI on the device and run the command "info/sys/ps" to clearly identify if a PSU is installed in each available slot. 
    can-with-snmp: true
    can-with-syslog: false

#! REMOTE::HTTP
url: /config/hwPowerSupplyStatus
protocol: HTTPS

#! PARSER::JSON


# Power supply states:
# singlePowerSupplyOk(1)
# firstPowerSupplyFailed(2)
# secondPowerSupplyFailed(3)
# doublePowerSupplyOk(4)
# singlePowerSupplyConnected (7)

# There are limitations to what Radware will provide in the output of the data for power supplies. 
# Very simply, it will only alert if the status of the PS is OK (values 1 or 4) or failed (values 2 and 3). 
# However, it is hard to determine anything else other than a single PS being operational (value 7). 
# Due to this, it is best not to show in live-config as the limitations of this script assumes the device has both PS active unless the output of the query indicates otherwise (values 2,3, or 7).

_metrics:
    -
        _groups:
            "$.[?(@.hwPowerSupplyStatus == 1)]":
                _tags:
                    "im.name":
                        _constant: "hardware-element-status"
                    "im.dstype.displayType":
                        _constant: "state"
                    "name":
                        _constant: "Primary Power Supply"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Power Supplies"
                _value.double:
                    _constant: "1.0"
    -
        _groups:
            "$.[?(@.hwPowerSupplyStatus == 2)]":
                _tags:
                    "im.name":
                        _constant: "hardware-element-status"
                    "im.dstype.displayType":
                        _constant: "state"
                    "name":
                        _constant: "Primary Power Supply"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Power Supplies"
                _value.double:
                    _constant: "0.0"
    -
        _groups:
            "$.[?(@.hwPowerSupplyStatus == 2)]":
                _tags:
                    "im.name":
                        _constant: "hardware-element-status"
                    "im.dstype.displayType":
                        _constant: "state"
                    "name":
                        _constant: "Secondary Power Supply"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Power Supplies"
                _value.double:
                    _constant: "1.0"
    -
        _groups:
            "$.[?(@.hwPowerSupplyStatus == 3)]":
                _tags:
                    "im.name":
                        _constant: "hardware-element-status"
                    "im.dstype.displayType":
                        _constant: "state"
                    "name":
                        _constant: "Primary Power Supply"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Power Supplies"
                _value.double:
                    _constant: "1.0"
    -
        _groups:
            "$.[?(@.hwPowerSupplyStatus == 3)]":
                _tags:
                    "im.name":
                        _constant: "hardware-element-status"
                    "im.dstype.displayType":
                        _constant: "state"
                    "name":
                        _constant: "Secondary Power Supply"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Power Supplies"
                _value.double:
                    _constant: "0.0"
    -
        _groups:
            "$.[?(@.hwPowerSupplyStatus == 4)]":
                _tags:
                    "im.name":
                        _constant: "hardware-element-status"
                    "im.dstype.displayType":
                        _constant: "state"
                    "name":
                        _constant: "Primary Power Supply"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Power Supplies"
                _value.double:
                        _constant: "1.0"
    -
        _groups:
            "$.[?(@.hwPowerSupplyStatus == 4)]":
                _tags:
                    "im.name":
                        _constant: "hardware-element-status"
                    "im.dstype.displayType":
                        _constant: "state"
                    "name":
                        _constant: "Secondary Power Supply"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Power Supplies"
                _value.double:
                        _constant: "1.0"
    -
        _groups:
            "$.[?(@.hwPowerSupplyStatus == 7)]":
                _tags:
                    "im.name":
                        _constant: "hardware-element-status"
                    "im.dstype.displayType":
                        _constant: "state"
                    "name":
                        _constant: "Primary Power Supply"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Power Supplies"
                _value.double:
                    _constant: "1.0"
    -
        _tags:
            "im.name":
                _constant: "power-supply-inserted"
            "name":
                _constant: "Power Supply Inserted"
            "im.dstype.displayType":
                _constant: "state"
        _temp:
            powerSupplyStatus:
                _value: "hwPowerSupplyStatus"
        _transform:
            _value.double: |
                {
                    if (temp("powerSupplyStatus") == 7){
                        print "0.0"
                    } else {
                        print "1.0"
                    }
                }