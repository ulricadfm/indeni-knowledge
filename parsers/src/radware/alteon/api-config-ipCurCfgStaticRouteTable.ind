#! META
name: radware-api-config-ipCurCfgStaticRouteTable
description: Retrieve the static IP addresses.
type: monitoring
monitoring_interval: 10 minute 
requires:
    os.name: "alteon-os"
    vendor: "radware"
    or:
        -
            vsx: "true"
        -
            standalone: "true"
#! COMMENTS
static-routing-table:
    why: |
        It is important that the routing is configured the same for all cluster members of the same cluster. Otherwise there can be downtime in the event of a failover.
    how: |
        This alert uses the Radware API to extract the configured routes for the device by running /config/IpCurCfgStaticRouteTable.
    without-indeni: |
        An administrator could log into the device and run a CLI command or view the static routes over the GUI. However, they would have to manually compare the routing tables of each device to ensure that the cluster does not have any mismatches.
    can-with-snmp: true
    can-with-syslog: false

#! REMOTE::HTTP
url: /config/IpCurCfgStaticRouteTable
protocol: HTTPS

#! PARSER::JSON
_vars:
    root: IpCurCfgStaticRouteTable
    name: Indx
_metrics:
    -
        _groups:
            ${root}:
                _tags:
                    "im.name":
                        _constant: "static-routing-table"                    
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Static Routes"
                    "im.dstype.displayType":
                        _constant: "string"
                    "im.identity-tags":
                        _constant: "name"
                    "name":
                        _value: ${name}
                _value.complex:
                    "next-hop":
                        _value: Gateway
                    "mask":
                        _value: Mask
                    "network":
                        _value: DestIp
        _value: complex-array