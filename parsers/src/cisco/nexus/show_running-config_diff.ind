#! META
name: nexus-show-running-config-diff
description: Nexus show running config diff
type: monitoring
monitoring_interval: 15 minutes
requires:
    vendor: cisco
    os.name: nxos

#! COMMENTS
config-unsaved:
    why: |
       Check if the latest config changes were saved. If the config has not been saved and the device reloads, any unsaved config changes will be lost.
    how: |
       This script logs into the Cisco Nexus switch using SSH and uses the "show running-config diff | count" command to check if there is a diff between the saved and running configuration. The output includes the number of unsaved lines.
    without-indeni: |
       It is not possible to poll this data through SNMP. Configuration changes are reported in Syslog, but the saved/unsaved state is not reported.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
show running-config diff | count


#! PARSER::AWK

# will be a number. 0: no diff, >0 diff
/^[0-9]+/ {

    diff_lines = $1
    if (diff_lines > 0) {
        unsaved = "1.0"
    } else {
        unsaved = "0.0"
    }
    writeDoubleMetricWithLiveConfig("config-unsaved", null, "gauge", 3600, unsaved, "Configuration Unsaved?", "boolean", "")
}
