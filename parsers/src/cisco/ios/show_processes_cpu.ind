#! META
name: ios-show-processes-cpu
description: Retrieve device processes and collect CPU consumption metrics
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: cisco
    os.name: ios

#! COMMENTS
processes-cpu:
    why: |
       Capture the per-process CPU utilization. This information can be used to troubleshoot the root cause of overall system high cpu conditions.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the output of the "show processes cpu" command. The output includes a table with all the processes and their respective CPU utilization.
    without-indeni: |
       It is possible to poll this data through SNMP but additional external logic would be required to analyze the information over time.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
show processes cpu

#! PARSER::AWK

#CPU utilization for five seconds: 0%/0%; one minute: 1%; five minutes: 1%
# PID Runtime(ms)   Invoked      uSecs   5Sec   1Min   5Min TTY Process 
#   1           0         3          0  0.00%  0.00%  0.00%   0 Chunk Manager    
#   2           0      1286          0  0.00%  0.01%  0.00%   0 Load Meter

BEGIN {
    location = 0
}

/CPU utilization/ {
    next
}

/(PID|Runtime|Invoked)/ {
    # getColumns(trim($0), "[ \t]+", columns)
    next
}
 
/[0-9].*%/ {
    
    location++
    
    # reset process
    process = ""
    
    pid = trim($1)
    run = trim($2)
    inv = trim($3)
    usec = trim($4)
    fivsec = trim($5); sub(/%/, "", fivsec)
    onemin = trim($6); sub(/%/, "", onemin)
    fivmin = trim($7); sub(/%/, "", fivmin)
    tty = trim($8)
    for (i=9; i <= NF; i++) {process = process " " trim($i)}; process = trim(process)

    # getColData will not return full process name, i.e. it splits at first space.
    # 
    # getColData will return only "Chunk" instead of "Chunk Manager"
    #
    
    #pid = getColData(trim($0), columns, "PID")
    #run = getColData(trim($0), columns, "Runtime(ms)")
    #inv = getColData(trim($0), columns, "Invoked")
    #usec = getColData(trim($0), columns, "uSecs")
    #fivsec = getColData(trim($0), columns, "5Sec")
    #onemin = getColData(trim($0), columns, "1Min")
    #fivemin = getColData(trim($0), columns, "5Min")
    #tty = getColData(trim($0), columns, "TTY")
    #process = getColData(trim($0), columns, "Process")
    
    # writeDebug(process ", " pid )
    
    process_entry[location, "pid"] = pid
    process_entry[location, "process"] = process
    process_entry[location, "value"] = fivmin
    
}

END{
    for (i=1; i <= location; i++) {
        metric_tags["name"] = process_entry[i, "pid"]
        metric_tags["process-name"] = process_entry[i, "process"]        
        writeDoubleMetric("process-cpu", metric_tags, "gauge", 60, process_entry[i, "value"])
    }
}