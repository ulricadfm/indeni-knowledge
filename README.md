# indeni Knowledge Repository #

This repository currently contains the Collector scripts (parsers) for indeni. In the future, it will contain the human-generated rules as well. We currently do not plan to include the machine learning rules.

### What is this repository for? ###

This repository allows everyone around the world to contribute knowledge to one another. All the code that makes it into the "master" branch will sent to indeni installations around the world. You can help someone on the other side of the planet!

### How do I get set up and contribute? ###

[Read the IKP document on getting set up and contributing](https://indeni.atlassian.net/wiki/pages/viewpage.action?pageId=63012870)

### License ###

The code in this repository is made available under the Apache 2 license. By contributing code to this repository you assert that you own the intellectual property to that code, and by committing the code you are giving up any intellectual property ownership and are making it available to use by anyone, for commercial and non-commercial reasons, without royalties, fees or any other requirements.

### Who do I talk to? ###

product@indeni.com